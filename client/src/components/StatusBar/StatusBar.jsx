import React from "react";
import "./StatusBar.scss";
import { History, Store } from "core";
/*eslint-disable */
import { AnalyseService, SelectionService } from "services";
import { debounce } from "lodash";
/*eslint-enable */

export default class StatusBar extends React.Component {
  render() {
    const { project } = Store.get("app");
    const total = {
      tables: project.schema.tables.length,
      columns: 0,
      connections: project.schema.connections.length,
      historyStep: History.getStep()
    };
    /*
    const detected = {
      tablesCase: AnalyseService.detectTableCase().method,
      tablesNumber: AnalyseService.detectTableNumber().method,
      columnsCase: AnalyseService.detectColumnCase().method
    };*/
    project.schema.tables.forEach(table => {
      total.columns += table.columns.length;
    });

    return (
      <div className="StatusBar">
        <div className="left">
          <span>Tables: {total.tables}</span>
          <span>Columns: {total.columns}</span>
          <span>Connections: {total.connections}</span>
          <span>Step: {History.getStep()}</span>
          <span>Selected: {SelectionService.total()}</span>
        </div>
        {this.props.children}
        {/* <div className="right">
          <span className="pill">{detected.tablesCase}</span>
          <span className="pill">{detected.tablesNumber}</span>
          <span className="pill">{detected.columnsCase}</span>
        </div> */}
      </div>
    );
  }
}
