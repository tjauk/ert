import React from "react";
import { Store } from "core";
import "./Connection.scss";

export default class Connection extends React.Component {
  constructor(props) {
    super(props);
    this.inputRef = React.createRef();
  }
  onClick = event => {
    console.log("Connection click", event, Store.get("app"));
    event.preventDefault();
    event.stopPropagation();
  };
  onDoubleClick = event => {
    console.log("Connection doubleclick", event);
    event.preventDefault();
    event.stopPropagation();
  };
  onChangeConnection = attribute => event => {
    this.props.column[attribute] = event.target.value;
    this.setState({ render: Math.random() });
  };

  render() {
    const { connection } = this.props;
    if (!connection) {
      return null;
    }
    const { start, end } = connection;
    // const { position, start, end } = connection;
    // const { x, y } = position;

    // const stage = Store.get("app.stage");
    // const p = stage.position;
    // const start = { x: x + 0, y: y + 0 };
    // const end = { x: x + 100, y: y + 100 };
    const strokeColor = "#cbcb41";
    // const lineType = "line";
    return (
      <>
        <path
          d={`M ${start.x} ${start.y} ${end.x} ${end.y}`}
          stroke={strokeColor}
          strokeWidth="2"
          fill="none"
        />
        <circle
          cx={start.x}
          cy={start.y}
          r="5"
          stroke={strokeColor}
          strokeWidth="4"
          fill={strokeColor}
        />
        <circle
          cx={end.x}
          cy={end.y}
          r="5"
          stroke={strokeColor}
          strokeWidth="4"
          fill={strokeColor}
        />
      </>
    );
  }
}
