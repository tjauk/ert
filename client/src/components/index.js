import Connection from "./Connection/Connection";
import Layer from "./Layer/Layer";
import Modal from "./Modal/Modal";
import Table from "./Table/Table";
import TableColumn from "./Columns/TableColumn";
import ToolBar from "./ToolBar/ToolBar";
import Stage from "./Stage/Stage";
import StatusBar from "./StatusBar/StatusBar";

const Comp = {
  Connection,
  Layer,
  Modal,
  Table,
  TableColumn,
  ToolBar,
  Stage,
  StatusBar
};
export {
  Connection,
  Modal,
  Layer,
  Table,
  TableColumn,
  ToolBar,
  Stage,
  StatusBar
};
export default Comp;
