import React from "react";
import Dimensions from "react-dimensions";
import bg from "assets/500px-Sudoku_template.svg.png";
// import bg2 from "assets/dot.png";

// konva
import { Stage } from "react-konva";

export class ResponsiveStage extends React.Component {
  state = {
    position: {
      x: 0,
      y: 0
    }
  };
  componentDidMount() {
    window.addEventListener("resize", this.onResize);
    window.addEventListener("wheel", this.onScroll);
    window.addEventListener("touchmove", this.onTouch);
  }
  componentWillUnmount() {
    window.removeEventListener("resize");
    window.removeEventListener("scroll");
  }
  onResize() {
    console.log(window.innerWidth, window.innerHeight);
  }
  onScroll(event) {
    const delta = Math.sign(event.deltaY);
    const pos = {
      x: event.layerX,
      y: event.layerY
    };
    console.info(pos, delta);
  }
  onTouch(event) {
    console.log(event);
    event.preventDefault();
    event.stopPropagation();
  }
  onDragMove = e => {
    this.setState({
      position: {
        x: e.target.attrs.x,
        y: e.target.attrs.y
      }
    });
  };
  render() {
    return (
      <Stage
        style={{ backgroundImage: "url(" + bg + ")" }}
        width={this.props.containerWidth - 10}
        height={this.props.containerHeight - 10}
        draggable
        onDragStart={this.onDragMove}
        onDragMove={this.onDragMove}
        onDragEnd={this.onDragMove}
      >
        {this.props.children}
      </Stage>
    );
  }
}

export default Dimensions()(ResponsiveStage);
