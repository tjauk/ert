import React from "react";
import Dimensions from "react-dimensions";
import bg from "assets/grid100px.png";
// import bg from "assets/500px-Sudoku_template.svg.png";
// import bg2 from "assets/dot.png";

export class Stage extends React.Component {
  state = {
    isDragging: false,
    offset: {
      x: 0,
      y: 0
    }
  };
  componentDidMount() {
    window.addEventListener("resize", this.onResize);
    window.addEventListener("wheel", this.onScroll);
  }
  componentWillUnmount() {
    window.removeEventListener("resize", this.onResize);
    window.removeEventListener("wheel", this.onScroll);
  }
  onResize() {
    console.log(window.innerWidth, window.innerHeight);
  }
  onScroll(event) {
    const delta = Math.sign(event.deltaY);
    const pos = {
      x: event.layerX,
      y: event.layerY
    };
    console.info(pos, delta);
  }
  onTouch(event) {
    console.log(event);
    event.preventDefault();
    event.stopPropagation();
  }
  onDragStart = event => {
    const { stage } = this.props;
    const { offset } = this.state;
    offset.x = stage.position.x - event.clientX;
    offset.y = stage.position.y - event.clientY;
  };
  onDragMove = event => {
    const { stage } = this.props;
    const { isDragging, offset } = this.state;
    if (isDragging) {
      stage.position = {
        x: offset.x + event.clientX,
        y: offset.y + event.clientY
      };
    }
  };
  onDragEnd = event => {};
  render() {
    const { stage } = this.props;
    const { position } = stage;
    const { x, y } = position;
    return (
      <div
        style={{
          position: "relative",
          backgroundImage: "url(" + bg + ")",
          backgroundRepeat: "repeat",
          backgroundPositionX: x,
          backgroundPositionY: y,
          width: "100%",
          height: "100%",
          overflow: "hidden"
        }}
        width={this.props.containerWidth - 10}
        height={this.props.containerHeight - 10}
        onMouseDown={this.onDragStart}
        onMouseMove={this.onDragMove}
        onMouseLeave={this.onDragMove}
        onMouseUp={this.onDragEnd}
      >
        {this.props.children}
      </div>
    );
  }
}

export default Dimensions()(Stage);
