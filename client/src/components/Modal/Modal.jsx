import React from "react";
import { Uid } from "core";
import "./Modal.scss";
import { Button } from "ui";

export default class Modal extends React.Component {
  state = {
    show: true
  };
  constructor(props) {
    super(props);
    this.uid = Uid.create();
  }

  open = e => {
    this.setState({ show: true });
  };

  close = e => {
    this.setState({ show: false });
  };

  onButtonOK = e => {
    const { onOK } = this.props;
    onOK ? onOK(e, this) : this.close();
  };

  onButtonCancel = e => {
    const { onCancel } = this.props;
    onCancel && onCancel(e, this);
    this.close();
  };

  render() {
    const { show } = this.state;
    const { type } = this.props;
    if (!show) {
      return null;
    }
    const classNames = ["Modal"];
    return (
      <div className="Modal-Holder" onClick={this.close}>
        <div
          className={classNames.join(" ")}
          onClick={e => e.stopPropagation()}
        >
          <div className="Modal-Header" />
          <div className="Modal-Body">{this.props.children}</div>
          {type === "Confirm" ? (
            <div className="Modal-Footer">
              <Button primary="true" onClick={this.onButtonOK}>
                OK
              </Button>
              <Button onClick={this.onButtonCancel}>Cancel</Button>
            </div>
          ) : (
            <div className="Modal-Footer">
              <Button primary="true">OK</Button>
            </div>
          )}
        </div>
      </div>
    );
  }
}
