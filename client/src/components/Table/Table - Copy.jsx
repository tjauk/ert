import React from "react";
import { Column } from "components";

// konva
import { Rect, Text, Group } from "react-konva";

export default class Table extends React.PureComponent {
  onClick = event => {
    const { table, onSelect } = this.props;
    onSelect(table);
  };
  onDragStart = event => {};
  onDragMove = event => {
    const { table, onChange } = this.props;
    table.position = {
      x: event.target.attrs.x,
      y: event.target.attrs.y
    };
    onChange(table);
  };
  onDragEnd = event => {
    const { table, onChange, onDone } = this.props;
    table.position = {
      x: event.target.attrs.x,
      y: event.target.attrs.y
    };
    onChange(table);
    onDone(table);
  };
  render() {
    const { table } = this.props;
    const { columns, position } = table;
    const { x, y } = position;
    const title = `${table.name} ${x},${y}`;
    const color = table.selected ? "#6386B0" : "#175177";
    return (
      <Group
        x={x}
        y={y}
        draggable
        onClick={this.onClick}
        onDragStart={this.onDragStart}
        onDragMove={this.onDragMove}
        onDragEnd={this.onDragEnd}
      >
        <Rect
          width={150}
          height={250}
          stroke={color}
          strokeWidth={2}
          fill={"#444"}
        />
        <Rect
          width={150}
          height={28}
          stroke={color}
          strokeWidth={1}
          fill={color}
        />
        <Text text={title} fill={"#fff"} fontStyle={"bold"} x={5} y={8} />
        {columns.map((column, nr) => (
          <Column
            key={nr}
            table={table}
            column={column}
            nr={nr}
            x={0}
            y={36 + nr * 20}
          />
        ))}
      </Group>
    );
  }
}
