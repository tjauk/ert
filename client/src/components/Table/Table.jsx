import React from "react";
import AutosizeInput from "react-input-autosize";
import { Drag, Focus, Ref, Store } from "core";
import { SelectionService, PredictionService, ProjectService } from "services";
import { TableColumn } from "components";
import "./Table.scss";

export class Table extends React.Component {
  constructor(props) {
    super(props);
    this.uid = `t${props.index}`;
    this.inputRef = Ref.create(`${this.uid}.input`);
    this.tableRef = Ref.create(`${this.uid}.table`);
  }

  componentDidUpdate() {
    if (this.tableRef.current) {
      /*
      const {
        x,
        y,
        width,
        height
      } = this.tableRef.current.getBoundingClientRect();
      console.log(x, y, width, height);
      */
    }
  }

  onMouseDown = event => {
    const { table } = this.props;
    Drag.start(this, event, table.position);
    event.stopPropagation();
  };

  onMouseMove = event => {
    const { table, index, onChange } = this.props;
    if (Drag.active(this.uid)) {
      table.position = Drag.move(event);
      const { width, height } = this.tableRef.current.getBoundingClientRect();
      table.position.width = width;
      table.position.height = height;
      onChange(table);
    }
    if (Drag.activeAndStarted(this.uid)) {
      if (!SelectionService.isMultiple()) {
        SelectionService.add(index);
      }
    }
    event.preventDefault();
  };

  onMouseUp = event => {
    const { table, index, onChange, onDone } = this.props;
    console.log("Table Mouse Up", table.name);
    if (this.inputRef.current) {
      // this.inputRef.current.focus();
    }
    if (Drag.activeAndStarted(this.uid)) {
      table.position = Drag.move(event);
      const { width, height } = this.tableRef.current.getBoundingClientRect();
      table.position.width = width;
      table.position.height = height;
      console.log("Position:", table.position);
      onChange(table);
      onDone(table);
    } else {
      SelectionService.toggle(index);
    }
    Drag.end(event);
    // event.preventDefault();
    // event.stopPropagation();
  };

  onClick = event => {
    console.log("Table click", Store.get("app"));
    if (this.inputRef.current) {
      this.inputRef.current.focus();
    }
    event.preventDefault();
    event.stopPropagation();
  };

  onDoubleClick = event => {
    console.log("Table doubleclick", event);
    if (this.inputRef.current) {
      this.inputRef.current.select();
    }
    event.preventDefault();
    event.stopPropagation();
  };

  onFocus = event => {
    // const { table, onChange } = this.props;
    // Focus.clearAll();
    // table.focus = true;
    // onChange(table);
  };

  onBlur = event => {
    Focus.clearAll();
  };

  onKeyDown = event => {
    var key = event.key || event.keyCode;
    console.log("Table onKeyDown", key);
    if (["Enter", "ArrowDown"].includes(key)) {
      const { table, onChange } = this.props;
      const { mode } = Store.get("app");
      if (mode === "SKETCH") {
        ProjectService.createNewTable();
      } else if (table.columns.length === 0) {
        this.createColumn();
      } else {
        Focus.clearAll();
        table.columns[0].focus = true;
        onChange(table);
      }
    }
  };

  changeAttribute = attribute => event => {
    const { table, onChange } = this.props;
    table[attribute] = event.target.value;
    onChange(table);
    event.preventDefault();
    event.stopPropagation();
    PredictionService.columnNames();
  };

  createColumn = () => {
    Focus.clearAll();
    const { table, onChange } = this.props;
    const last = table.columns[table.columns.length - 1];
    if ((last && last.name !== "") || table.columns.length === 0) {
      const column = {};
      column.edit = true;
      column.name = "";
      column.focus = true;
      table.columns.push(column);
      onChange(table);
    }
  };

  createColumnAfter = index => {
    const { table, onChange } = this.props;
    const column = {};
    column.edit = true;
    column.name = "";
    column.focus = true;
    table.columns.map(c => (c.focus = false));
    table.columns = [
      ...table.columns.splice(0, index + 1),
      column,
      ...table.columns.splice(0)
    ];
    onChange(table);
  };

  removeColumn = index => {
    const { table, onChange } = this.props;
    table.columns = [
      ...table.columns.splice(0, index),
      ...table.columns.splice(1)
    ];
    onChange(table);
  };

  render() {
    const { mode } = Store.get("app");
    const { table } = this.props;
    const { columns, position } = table;
    const { x, y } = position;
    // const coords = `${x},${y}`;
    const classNames = ["Table"];
    if (table.selected) {
      classNames.push("selected");
    }

    classNames.push("mode" + mode); // "SKETCH","SIMPLE","FULL"

    if (Drag.active(this.uid)) {
      classNames.push("dragging");
    }
    const className = classNames.join(" ");

    if (table.focus && this.inputRef.current) {
      this.inputRef.current.focus();
    }

    return (
      <div
        className={className}
        style={{
          position: "absolute",
          left: x,
          top: y,
          zIndex: Drag.active(this.uid) ? 9999 : "auto"
        }}
        onMouseDown={this.onMouseDown}
        onMouseMove={this.onMouseMove}
        onMouseLeave={this.onMouseMove}
        onMouseUp={this.onMouseUp}
      >
        {Drag.activeAndStarted(this.uid) && <div className="draggable-area" />}
        <table ref={this.tableRef} style={{ width: "100%" }}>
          <thead>
            <tr>
              <td
                colSpan={mode === "FULL" ? 4 : 1}
                onClick={this.onClick}
                onDoubleClick={this.onDoubleClick}
              >
                <AutosizeInput
                  type="text"
                  placeholder="Table name"
                  ref={this.inputRef}
                  value={table.name}
                  onChange={this.changeAttribute("name")}
                  onKeyDown={this.onKeyDown}
                  onFocus={this.onFocus}
                  onBlur={this.onBlur}
                />
              </td>
            </tr>
          </thead>
          <tbody>
            {(mode === "SIMPLE" || mode === "FULL") &&
              columns.map((column, index) => (
                <TableColumn
                  key={index}
                  index={index}
                  table={table}
                  parent={this}
                  column={column}
                  mode={mode}
                />
              ))}
            {mode !== "SKETCH" && (
              <tr>
                <td>
                  <span onClick={this.createColumn}>+ add new</span>
                </td>
              </tr>
            )}
          </tbody>
        </table>
      </div>
    );
  }
}

export default Table;
