import React from "react";
import { Drag, Store, Uid } from "core";
import "./Layer.scss";
import { SelectionService } from "services";

export default class Layer extends React.Component {
  constructor(props) {
    super(props);
    this.uid = Uid.create();
  }

  onMouseDown = event => {
    const { stage } = this.props;
    Drag.start(this, event, stage.position);
  };

  onMouseMove = event => {
    const { stage, onChange } = this.props;
    if (Drag.active(this.uid)) {
      stage.position = Drag.move(event);
      onChange(stage);
    }
  };

  onMouseUp = event => {
    const { stage, onChange, onDone } = this.props;
    if (Drag.active(this.uid)) {
      if (!Drag.activeAndStarted(this.uid)) {
        SelectionService.clear();
      }
      stage.position = Drag.move(event);
      Drag.end(event);
      onChange(stage);
      onDone(stage);
    }
  };

  tempDragEnd = event => {
    const { stage, onChange, onDone } = this.props;
    if (Drag.active(this.uid)) {
      stage.position = Drag.move(event);
      Drag.end(event);
      if (Drag.activeAndStarted(this.uid)) {
        // stage moved
      } else {
        SelectionService.clear();
      }
      onChange(stage);
      onDone(stage);
    }
  };

  render() {
    const { stage } = Store.get("app");
    const { x, y } = stage.position;
    const scale = stage.zoom;
    //
    const classNames = ["Layer"];
    if (Drag.active(this.uid)) {
      classNames.push("dragging");
    }
    const className = classNames.join(" ");
    return (
      <div
        className={className}
        onMouseDown={this.onMouseDown}
        onMouseMove={this.onMouseMove}
        onMouseLeave={this.onMouseMove}
        onMouseUp={this.onMouseUp}
      >
        <div
          className={className}
          style={{
            position: "absolute",
            width: 0,
            height: 0,
            left: x,
            top: y,
            transform: "scale(" + scale + ")"
          }}
        >
          {this.props.children}
        </div>
      </div>
    );
  }
}
