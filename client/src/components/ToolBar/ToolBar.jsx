import React from "react";
import { Store } from "core";
import { HistoryService, ProjectService, SelectionService } from "services";
import { Button, Icon } from "ui";
import "./ToolBar.scss";

export default class ToolBar extends React.Component {
  changeViewMode(mode) {
    Store.set("app.mode", mode);
  }

  duplicateSelectedTables() {
    const app = Store.get("app");
    const selected_tables = app.project.schema.tables.filter(
      table => table.selected
    );
    selected_tables.map(table => {
      table.name = table.name + " copy";
      app.project.schema.tables.push(table);
    });
    Store.set("app", app);
  }

  deleteSelectedTables() {
    const app = Store.get("app");
    app.project.schema.tables = app.project.schema.tables.filter(
      table => !table.selected
    );
    app.selected = [];
    Store.set("app", app);
  }

  render() {
    const app = Store.get("app");
    return (
      <div className="ToolBar">
        <Button onClick={() => HistoryService.undo()}>
          <Icon fa="undo" /> Undo
        </Button>
        <Button onClick={() => HistoryService.redo()}>
          <Icon fa="redo" />
          Redo
        </Button>
        <span
          style={{ paddingRight: "10px", minWidth: "40px", textAlign: "right" }}
        >
          New:
        </span>
        <Button onClick={() => ProjectService.createNewTable()}>Table</Button>
        <span
          style={{
            paddingRight: "10px",
            minWidth: "120px",
            textAlign: "right"
          }}
        >
          View mode: {app.mode}
        </span>
        <Button onClick={() => this.changeViewMode("SKETCH")}>Sketch</Button>
        <Button onClick={() => this.changeViewMode("SIMPLE")}>Simple</Button>
        <Button onClick={() => this.changeViewMode("FULL")}>Full</Button>
        {SelectionService.total() > 0 && (
          <>
            <Button onClick={() => SelectionService.clear()}>Clear</Button>
            <Button onClick={() => this.duplicateSelectedTables()}>
              Duplicate
            </Button>
            <Button onClick={() => this.deleteSelectedTables()}>Delete</Button>
          </>
        )}
        {this.props.children}
      </div>
    );
  }
}
