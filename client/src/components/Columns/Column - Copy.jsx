import React from "react";

// konva
import { Text, Line } from "react-konva";
import { CanvasInput } from "components";
import { isThisHour } from "date-fns";

export default class Column extends React.Component {
  state = {
    edit: false
  };
  onClick = event => {
    console.log("Column click", event);
  };
  onDoubleClick = event => {
    console.log("Column doubleclick", event);
    this.setState({ edit: !this.state.edit });
  };

  render() {
    const { table, column, x, y } = this.props;
    const { edit } = this.state;
    return (
      <>
        {edit && <input value={column.name} x={x + 8} y={y} fill={"#77f"} />}
        {!edit && (
          <Text
            text={column.name}
            x={x + 8}
            y={y}
            fill={"#fff"}
            onClick={this.onClick}
            onDblClick={this.onDoubleClick}
            onDblTap={this.onDoubleClick}
          />
        )}
        <Line
          points={[x, y + 16, 150, y + 16]}
          tension={0}
          stroke={"#6386B0"}
          strokeWidth={1}
        />
      </>
    );
  }
}
