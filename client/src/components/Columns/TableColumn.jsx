import React from "react";
import { Focus, Key, Ref, Store } from "core";
import AutosizeInput from "react-input-autosize";
import { Select } from "ui";
import { orderBy } from "lodash";
import "./TableColumns.scss";

const mysqlDataTypes = [
  // string
  "CHAR",
  "VARCHAR",
  "BINARY",
  "VARBINARY",
  "TINYBLOB",
  "TINYTEXT",
  "TEXT",
  "BLOB",
  "MEDIUMTEXT",
  "MEDIUMBLOB",
  "LONGTEXT",
  "LONGBLOB",
  "ENUM",
  "SET",
  // numeric
  "BIT",
  "TINYINT",
  "BOOL",
  "BOOLEAN",
  "SMALLINT",
  "MEDIUMINT",
  "INT",
  "INTEGER",
  "BIGINT",
  "FLOAT",
  "DOUBLE",
  "DECIMAL",
  "DEC",
  // date and time
  "DATE",
  "DATETIME",
  "TIMESTAMP",
  "TIME",
  "YEAR",
  // misc
  "JSON",
  // spatial
  "GEOMETRY",
  "POINT",
  "LINESTRING",
  "POLYGON",
  "MULTIPOINT",
  "MULTILINESTRING",
  "MULTIPOLYGON",
  "GEOMETRYCOLLECTION"
];

export default class TableColumn extends React.Component {
  constructor(props) {
    super(props);
    this.uid = `t${props.parent.props.index}.c${props.index}`;
    this.nameRef = Ref.create(`${this.uid}.name`);
    this.typeRef = Ref.create(`${this.uid}.type`);
    this.sizeRef = Ref.create(`${this.uid}.size`);
  }

  onClick = event => {
    console.log("TableColumn click", Store.get("app"));
    if (this.nameRef.current) {
      this.nameRef.current.focus();
    }
    event.preventDefault();
    event.stopPropagation();
  };

  onDoubleClick = attribute => event => {
    console.log("TableColumn doubleclick", event);
    if (attribute === "name" && this.nameRef.current) {
      this.nameRef.current.select();
    }
    if (attribute === "size" && this.sizeRef.current) {
      this.sizeRef.current.select();
    }
    event.preventDefault();
    event.stopPropagation();
  };

  onFocus = event => {
    //
  };

  onBlur = event => {
    Focus.clearAll();
  };

  onChangeColumn = attribute => event => {
    this.props.column[attribute] = event.target.value;
    this.setState({ render: Math.random() });
    /*
    event.stopPropagation();
    const { parent, column, index } = this.props;
    const { table } = parent.props;
    column[attribute] = event.target.value;
    table.columns[index] = column;
    parent.props.onChange(table);
    */
  };

  onKeyPress = event => {
    var key = event.key || event.keyCode;
    const { parent } = this.props;
    if (key === "Enter" || key === "Tab") {
      parent.createColumn();
    }
  };

  onKeyDown = event => {
    var key = event.key || event.keyCode;
    console.log(Key.pressed, key);
    const { parent, index } = this.props;
    const { table, onChange } = parent.props;

    if (Key.map["Control"] && Key.map["Enter"]) {
      table.columns[index].focus = false;
      parent.createColumnAfter(index);
    } else if (["Enter", "ArrowDown"].includes(key)) {
      table.columns[index].focus = false;
      if (table.columns[index + 1]) {
        table.columns[index + 1].focus = true;
        onChange(table);
      } else {
        parent.createColumn();
      }
    }
    if ("ArrowUp" === key) {
      table.columns[index].focus = false;
      if (table.columns[index].name === "") {
        parent.removeColumn(index);
      }
      if (table.columns[index - 1]) {
        table.columns[index - 1].focus = true;
      } else {
        table.focus = true;
      }
      onChange(table);
    }
  };

  render() {
    const { column, mode } = this.props;
    const edit = true;

    column.type = column.type || "string";
    column.primary = column.primary || false;

    if (column.focus && this.nameRef.current) {
      this.nameRef.current.focus();
    }

    return (
      <tr className="TableColumn">
        <td onClick={this.onClick}>
          {edit ? (
            <AutosizeInput
              type="text"
              ref={this.nameRef}
              value={column.name}
              placeholder={"Column name"}
              onKeyDown={this.onKeyDown}
              onChange={this.onChangeColumn("name")}
              onDoubleClick={this.onDoubleClick("name")}
              onFocus={this.onFocus}
              onBlur={this.onBlur}
              autoFocus={column.focus === true}
            />
          ) : (
            column.name
          )}
        </td>
        {mode === "FULL" && (
          <>
            <td>
              <Select
                className={"ui-select"}
                ref={this.typeRef}
                value={column.type}
                options={orderBy(mysqlDataTypes)}
                onChange={this.onChangeColumn("type")}
              />
            </td>
            <td>
              <AutosizeInput
                type="text"
                ref={this.sizeRef}
                className={"column-size"}
                value={column.size}
                placeholder={"size"}
                onKeyDown={this.onKeyDown}
                onChange={this.onChangeColumn("size")}
                onDoubleClick={this.onDoubleClick("size")}
                onFocus={this.onFocus}
                onBlur={this.onBlur}
              />
            </td>
            <td>{column.primary ? "Yes" : "No"}</td>
          </>
        )}
      </tr>
    );
  }
}
