import { Entity } from './Entity';
import { Project } from './Project';
import { Schema } from './Schema';
import { Table } from './Table';

const Model = {
    Entity,
    Project,
    Schema,
    Table
}
export {
    Model
}
export default Model;