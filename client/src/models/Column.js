export const Column = {
  name: "id",
  type: "string",
  size: 191,
  //
  default: "",
  primary: false,
  nullable: true
};
