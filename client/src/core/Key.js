class Key {
  static pressed = null;
  static released = null;
  static map = {};

  static log() {
    document.addEventListener("keydown", event => {
      const key = event.key || event.keyCode;
      const type = event.type;
      Key.pressed = key;
      Key.map[key] = type === "keydown";
      console.log(Key.map);
    });
    document.addEventListener("keyup", event => {
      const key = event.key || event.keyCode;
      const type = event.type;
      Key.pressed = null;
      Key.map[key] = type === "keydown";
      console.log(Key.map);
    });
  }

  static down(key = null) {
    return key === key.pressed;
  }

  static up(key = null) {
    return key === key.released;
  }

  static ANY() {
    return Key.pressed !== null;
  }
  static CTRL() {
    return Key.pressed === "Control";
  }
  static SHIFT() {
    return Key.pressed === "Shift";
  }
}

export default Key;
