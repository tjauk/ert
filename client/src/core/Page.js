import { Store } from "core";

export default class Page {
  static middlewares = [];

  static open(page) {
    Store.set("app.page", page);
  }

  // @todo
  static build(params) {
    const query = params;
    return query;
  }
}
