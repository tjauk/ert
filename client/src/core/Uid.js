export default class Uid {
  static autoincrement = 0;

  static get(name = null) {
    const prefix = name ? name : "uid-";
    this.autoincrement++;
    return prefix + this.autoincrement.toString();
  }

  // alias
  static create(name = null) {
    return this.get(name);
  }
}
