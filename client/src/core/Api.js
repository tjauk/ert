import axios from "axios";

const API_URL = process.env.REACT_APP_API_URL;

const defaults = {
  // `baseURL` will be prepended to `url` unless `url` is absolute.
  // It can be convenient to set `baseURL` for an instance of axios to pass relative URLs
  // to methods of that instance.
  baseURL: API_URL,

  // `timeout` specifies the number of milliseconds before the request times out.
  // If the request takes longer than `timeout`, the request will be aborted.
  timeout: 0, // default is `0` (no timeout)

  // `withCredentials` indicates whether or not cross-site Access-Control requests
  // should be made using credentials
  withCredentials: false, // default

  // `responseType` indicates the type of data that the server will respond with
  // options are: 'arraybuffer', 'document', 'json', 'text', 'stream'
  //   browser only: 'blob'
  responseType: "json" // default
};

export default class Api {
  static headers = [];
  static middlewares = [];
  // @todo
  static build(params) {
    const query = params;
    return query;
  }
  // merge defaults and params and create config
  static config(params) {
    let config = defaults;
    for (const key in params) {
      if (params.hasOwnProperty(key)) {
        config[key] = params[key];
      }
    }
    return config;
  }
  // GET
  static get(url, params = {}) {
    return axios.get(url, this.config(params));
  }
  // POST
  static post(url, data = {}, params = {}) {
    return axios.post(url, data, this.config(params));
  }
  // PUT
  static put(url, data = {}, params = {}) {
    return axios.put(url, data, this.config(params));
  }
  // PATCH
  static patch(url, data = {}, params = {}) {
    return axios.patch(url, data, this.config(params));
  }
  // DELETE
  static delete(url, params = {}) {
    return axios.delete(url, this.config(params));
  }
}
