class Model {
  constructor() {
    this.attributes = {};
  }
  get name() {
    return this.attributes.name;
  }
  set name(str) {
    this.attributes.name = str;
  }
}

class Person extends Model {
  get fullname() {
    return this.attributes.name + " " + this.attributes.lastname;
  }
  set lastname(str) {
    this.attributes.lastname = str;
  }
}

const p = new Person();

console.log(p);

p.name = "First";
p.lastname = "Last";
console.log(p);

console.log(p.fullname);

/*
//shorter version (taken from "You don't know js" book, Kyle Simpson)
let pobj = new Proxy( {}, {
  get() {
    throw "No such property/method!";
  },
  set() {
    throw "No such property/method!";
  }
});

let obj = {
  a: 1,
  foo() {
    console.log( "a:", this.a );
  }
};

// setup `obj` to fall back to `pobj`
Object.setPrototypeOf( obj, pobj );

obj.a = 3;
obj.foo();          // a: 3

obj.b = 4;          // Error: No such property/method!
obj.bar();          // Error: No such property/method!

*/
