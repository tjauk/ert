const APP_NAME = process.env.REACT_APP_NAME;

export default class Storage {
  static load(name) {
    return JSON.parse(window.localStorage.getItem(APP_NAME + "_" + name));
  }

  static save(name, data) {
    window.localStorage.setItem(APP_NAME + "_" + name, JSON.stringify(data));
  }
}
