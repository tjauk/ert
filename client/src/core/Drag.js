export default class Drag {
  static startedDragging = false;
  static draggingUid = null;
  static tolerance = 10;
  static snapGridSize = 2;
  static initial = {
    x: 0,
    y: 0
  };
  static offset = {
    x: 0,
    y: 0
  };
  //
  static active(uid = null) {
    if (uid && uid === this.draggingUid) {
      return true;
    }
    return false;
  }
  static activeAndStarted(uid = null) {
    if (uid && uid === this.draggingUid && this.startedDragging) {
      return true;
    }
    return false;
  }
  static start(component, mouseEvent, draggingItem) {
    if (!component.uid) {
      console.log("No uid provided on component");
    }

    this.draggingUid = component.uid;
    this.startedDragging = false;
    const mouse = {
      x: mouseEvent.clientX,
      y: mouseEvent.clientY
    };
    this.initial = mouse;
    this.offset = {
      x: draggingItem.x - mouse.x,
      y: draggingItem.y - mouse.y
    };
  }
  static move(mouseEvent) {
    const mouse = {
      x: mouseEvent.clientX,
      y: mouseEvent.clientY
    };
    // did not start dragging yet
    const t = this.tolerance;
    if (!this.startedDragging) {
      if (
        mouse.x - t < this.initial.x ||
        mouse.x + t > this.initial.x ||
        mouse.y - t < this.initial.y ||
        mouse.y + t > this.initial.y
      ) {
        this.startedDragging = true;
      }
    }
    return {
      x: Drag.snap(Drag.offset.x + mouse.x),
      y: Drag.snap(Drag.offset.y + mouse.y)
    };
  }
  static end(mouseEvent) {
    this.startedDragging = false;
    this.draggingUid = null;
  }
  static snap(num) {
    return Math.round(num / Drag.snapGridSize) * Drag.snapGridSize;
  }
}
