export default class Store {
  static __current = null;
  static __components = {};
  //
  static __nextId = 1;
  //
  static use(name) {
    this.__current = name;
  }

  static get app() {
    return this.get("app");
  }
  static set app(value) {
    this.set("app", value);
  }

  static register(component, name, initialState = null) {
    // console.log("Store.register", name, component.state, initialState);
    if (initialState) {
      // next time
      if (this.__components[name] && this.__components[name].state) {
        component.state = this.__components[name].state;
      } else {
        component.state = initialState;
      }
    }

    this.__components[name] = component;
  }

  static get(name = null) {
    if (name.indexOf(".") > 0) {
      return this.getByDots(name);
    }
    if (this.__components[name]) {
      return this.__components[name].state;
    }
    return null;
  }

  static set(name = null, value = null) {
    if (name.indexOf(".") > 0) {
      this.setByDots(name, value);
    }
    if (this.__components[name]) {
      this.__components[name].setState(value);
    }
  }

  static dottedParts(str) {
    return str.split(".");
  }

  static getByDots(dotpath = null) {
    const parts = this.dottedParts(dotpath);
    const name = parts.shift();
    if (this.__components[name]) {
      let part = null;
      let dig = this.__components[name].state;
      while ((part = parts.shift())) {
        if (dig[part]) {
          dig = dig[part];
        } else {
          return null;
        }
      }
      return dig;
    }
    return null;
  }

  static setByDots(dotpath = null, value = null) {
    const parts = this.dottedParts(dotpath);
    const name = parts.shift();
    if (this.__components[name]) {
      let state = this.__components[name].state;
      if (parts[5]) {
        state[parts[0]][parts[1]][parts[2]][parts[3]][parts[4]][
          parts[5]
        ] = value;
      } else if (parts[4]) {
        state[parts[0]][parts[1]][parts[2]][parts[3]][parts[4]] = value;
      } else if (parts[3]) {
        state[parts[0]][parts[1]][parts[2]][parts[3]] = value;
      } else if (parts[2]) {
        state[parts[0]][parts[1]][parts[2]] = value;
      } else if (parts[1]) {
        state[parts[0]][parts[1]] = value;
      } else if (parts[0]) {
        state[parts[0]] = value;
      } else {
        state = value;
      }
      this.__components[name].setState(state);
    }
  }

  static getId() {
    const id = this.__nextId;
    this.__nextId++;
    return id;
  }
}
