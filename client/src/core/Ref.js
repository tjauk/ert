import React from "react";

export default class Ref {
  static refs = {};

  static create(id) {
    if (Ref.refs[id]) {
      return Ref.refs[id];
    }
    Ref.refs[id] = React.createRef();
    return Ref.refs[id];
  }

  static get(id) {
    return Ref.refs[id];
  }
}
