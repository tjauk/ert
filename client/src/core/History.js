export default class History {
  static history = [];
  static historyStep = 0;
  static historyMax = 100;

  static getStep() {
    return this.historyStep;
  }

  static getTotal() {
    return this.history.length;
  }

  static addToHistory(project) {
    this.history = this.history.slice(0, this.historyStep + 1);
    this.history.push(JSON.parse(JSON.stringify(project)));
    if (this.history.length > this.historyMax) {
      this.history.shift();
    }
    this.historyStep = this.history.length - 1;
  }

  static undoProject(project) {
    if (this.history.length > 0 && this.historyStep - 1 > -1) {
      this.historyStep--;
      project = JSON.parse(JSON.stringify(this.history[this.historyStep]));
    }
    return project;
  }

  static redoProject(project) {
    if (this.history[this.historyStep + 1]) {
      this.historyStep++;
      project = this.history[this.historyStep];
    }
    return project;
  }
}
