export default class Selection {
  static items = [];

  static set(item) {
    if (Array.isArray(item)) {
      this.items = item;
    } else {
      this.items = [item];
    }
  }
  static add(item) {
    this.items.push(item);
  }
  static clear() {
    this.items = [];
  }
  static exists(item) {
    // @todo
  }
  static all() {
    return this.items;
  }
  static first() {
    return this.items[0] ? this.items[0] : null;
  }
  static last() {
    const len = this.items.length;
    return this.items[len - 1];
  }
}
