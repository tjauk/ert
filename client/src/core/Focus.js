import { Ref, Store } from "core";

export default class Focus {
  static item = null;
  static previous = null;

  static clearAll() {
    const { project } = Store.get("app");
    project.schema.tables.map(table => {
      table.focus = false;
      table.columns.map(column => (column.focus = false));
      return table;
    });
    Store.set("app.project", project);
  }

  static id(id) {
    const item = Ref.get(id);
    if (item.current) {
      item.current.focus();
      Focus.set(item);
    }
  }

  static set(item) {
    this.previous = this.item;
    this.item = item;
  }
  static get() {
    return this.item;
  }
  static getPrevious() {
    return this.previous;
  }
}
