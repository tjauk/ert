import React from "react";

import { Api, Store } from "core";

// ui
import { Button, Column, Modal, Panel, PanelLink, Scrollable } from "ui";

// components
import { Table, ToolBar } from "components";

// services
import { ImportService } from "services";

const testString = `
CREATE TABLE 'countries' (
	'id' INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	'name' VARCHAR(45) NOT NULL DEFAULT '' COLLATE 'utf8_unicode_ci',
	'code' VARCHAR(2) NOT NULL DEFAULT '' COLLATE 'utf8_unicode_ci',
	'pri' SMALLINT(6) NOT NULL DEFAULT '0',
	'iso3166' VARCHAR(2) NOT NULL DEFAULT '' COLLATE 'utf8_unicode_ci',
	'iso3166a3' VARCHAR(3) NOT NULL DEFAULT '' COLLATE 'utf8_unicode_ci',
	'drzava_uid' VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	'drzava_b2b' VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	'shippable' TINYINT(4) NOT NULL DEFAULT '0',
	PRIMARY KEY ('id')
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
AUTO_INCREMENT=253
;
`;

export class ImportPage extends React.Component {
  initialState = {
    sql: testString,
    result: ""
  };

  constructor(props) {
    super(props);
    Store.register(this, "importpage", this.initialState);
    this.textRef = React.createRef();
  }

  // methods
  handleChange = event => {
    this.setState({ sql: event.target.value });
  };

  handleImport = event => {
    const { sql } = this.state;
    this.setState({ preview: "Importing..." });
    ImportService.text(sql).then(result => {
      this.setState({ preview: JSON.stringify(result, null, 2) });
    });
  };

  render() {
    const { sql, preview } = this.state;
    return (
      <Column
        style={{
          display: "flex",
          flexDirection: "row",
          minHeight: "100%"
        }}
      >
        <Column style={{ height: "100%" }}>
          <textarea
            style={{
              height: "300px",
              color: "#0f0",
              background: "#000",
              border: "none"
            }}
            ref={this.textRef}
            value={sql}
            onChange={this.handleChange}
          />
          <Button onClick={this.handleImport}>Import test</Button>
        </Column>
        <Column
          style={{
            display: "flex",
            flexDirection: "row",
            minHeight: "100%"
          }}
        >
          <Scrollable>
            <pre>{preview}</pre>
          </Scrollable>
        </Column>
      </Column>
    );
  }
}
