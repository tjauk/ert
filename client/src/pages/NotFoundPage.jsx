import React from "react";

// ui
import { Column } from "ui";

export class NotFoundPage extends React.PureComponent {
  render() {
    return (
      <Column
        style={{ display: "flex", flexDirection: "row", minHeight: "100%" }}
      >
        <h1>Page not found</h1>
      </Column>
    );
  }
}
