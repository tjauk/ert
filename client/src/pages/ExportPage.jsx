/*eslint-disable */

import React from "react";

import { Api, Keyboard, Store } from "core";

// ui
import { Column, Layout, Panel, PanelLink, Scrollable } from "ui";

// components
import { Layer, Stage, StatusBar, Table, ToolBar } from "components";

// services
import { HistoryService, ProjectService } from "services";

/*eslint-enable */

export class ExportPage extends React.Component {
  initalState = {
    exportMethod: null
  };

  constructor(props) {
    super(props);
    Store.register(this, "exportpage", this.initalState);
  }

  componentDidMount() {
    //
  }

  componentWillUnmount() {
    //
  }

  exportTo = method => event => this.setState({ exportMethod: method });

  render() {
    const { schema } = Store.get("app.project");
    const json = JSON.stringify(schema, null, 2);
    return (
      <>
        <Column className="Panels">
          <Panel name="Export to" expanded="true">
            <PanelLink name="SQL" onClick={this.exportTo("SQL")} />
            <PanelLink name="API" onClick={this.exportTo("API")} />
            <PanelLink name="PNG" onClick={this.exportTo("PNG")} />
            <PanelLink name="DOC" onClick={this.exportTo("DOC")} />
            <PanelLink name="PDF" onClick={this.exportTo("PDF")} />
            <PanelLink name="HTML" onClick={this.exportTo("HTML")} />
            <PanelLink name="JSON" onClick={this.exportTo("JSON")} />
            <PanelLink name="CSV" onClick={this.exportTo("CSV")} />
          </Panel>
        </Column>
        <Column
          style={{
            display: "flex",
            flexDirection: "row",
            minHeight: "100%"
          }}
        >
          <Column style={{ height: "100%" }}>
            <h1>Export Page Component</h1>
            <h2>{this.state.exportMethod}</h2>
          </Column>
          <Column
            style={{
              display: "flex",
              flexDirection: "row",
              minHeight: "100%"
            }}
          >
            <Scrollable>
              <pre>{json}</pre>
            </Scrollable>
          </Column>
        </Column>
      </>
    );
  }
}
