/*eslint-disable */

import React from "react";
import AutosizeInput from "react-input-autosize";

import { Api, Keyboard, Store } from "core";

// ui
import {
  Button,
  Column,
  Icon,
  Input,
  Panel,
  PanelLink,
  Select,
  Scrollable
} from "ui";

// components
import { Layer, Stage, StatusBar, Table, ToolBar } from "components";

// services
import { HistoryService, ProjectService } from "services";

/*eslint-enable */

const stringOptions = "A,B,C,D";
const rangeOptions = ["1.", "2.", "3.", "4."];
const fullOptions = [
  { value: 1, label: "One" },
  { value: 2, label: "Two" },
  { value: 3, label: "Three" }
];

export class TestPage extends React.Component {
  initialState = {
    input1: "AutosizeInput",
    input2: "Input 2",
    input3: "Input 3",
    select1: 1,
    select2: 2,
    select3: "B"
  };

  constructor(props) {
    super(props);
    Store.register(this, "testpage", this.initialState);
  }

  componentDidMount() {
    //
  }

  componentWillUnmount() {
    //
  }

  change = attribute => e => {
    Store.set("testpage", { [attribute]: e.target.value });
  };

  render() {
    const { input1, input2, select1, select2, select3, checkbox1 } = Store.get(
      "testpage"
    );
    return (
      <Column
        style={{ display: "flex", flexDirection: "row", minHeight: "100%" }}
      >
        <Column style={{ height: "100%" }}>
          <Scrollable>
            <h1>H1 Main Title</h1>
            <p>Lorem ipsum dolor sit amet</p>
            <h2>H2 Section Title</h2>
            <p>Lorem ipsum dolor sit amet</p>
            <h3>H3 Paragraph Title</h3>
            <p>Lorem ipsum dolor sit amet</p>

            <label>Autosize Input</label>
            <AutosizeInput value={input1} onChange={this.change("input1")} />

            <label>Input</label>
            <Input value={input2} onChange={this.change("input2")} />

            <label>Input placeholder</label>
            <Input placeholder="Placeholder" />

            <label>Select with label,value options</label>
            <Select
              options={fullOptions}
              value={select1}
              onChange={this.change("select1")}
            />

            <label>Select with rangeOptions</label>
            <Select
              options={rangeOptions}
              value={select2}
              onChange={this.change("select2")}
            />

            <label>Select with stringOptions A,B,C,D</label>
            <Select
              options={stringOptions}
              value={select3}
              onChange={this.change("select3")}
            />

            <label>Select full width</label>
            <Select
              options={stringOptions}
              value={select3}
              onChange={this.change("select3")}
              style={{ width: "auto" }}
            />

            <hr />

            <label>Button</label>
            <Button onClick={e => console.log(e)}>Button</Button>
            <label>Button with Icon</label>
            <Button onClick={e => console.log(e)}>
              <Icon fa="undo" />
              Button
            </Button>
            <label>Button with Icon after</label>
            <Button onClick={e => console.log(e)}>
              Button
              <Icon fa="undo" />
            </Button>

            <hr />
            <label>Checkbox</label>
            <Input
              type="checkbox"
              value={checkbox1}
              onChange={this.change("checkbox2")}
            />

            <label>Radio</label>
            <Input type="radio" />
          </Scrollable>
        </Column>
        <Column>
          <Scrollable>
            <pre>{JSON.stringify(this.state, null, 2)}</pre>
          </Scrollable>
        </Column>
        >
      </Column>
    );
  }
}
