import React from "react";

import { Store } from "core";

// ui
import { Column, Panel, PanelLink } from "ui";

// services
import {
  AnalyseService,
  ArrangeService,
  CaseService,
  HistoryService,
  ProjectService
} from "services";

export class StagePagePanels extends React.Component {
  render() {
    const { parent } = this.props;
    const { projects } = Store.get("app");
    return (
      <>
        <Column className="Panels">
          <Panel name="My Projects" expanded="true">
            {projects.map(proj => (
              <PanelLink
                key={proj.id}
                name={proj.name}
                onClick={() => ProjectService.load(proj.id)}
              />
            ))}
          </Panel>

          <Panel name="Case Converter">
            <PanelLink
              name="Table names &raquo; singular"
              onClick={() => CaseService.transform("tables", "singular")}
            />
            <PanelLink
              name="Table names &raquo; plural"
              onClick={() => CaseService.transform("tables", "plural")}
            />
            <PanelLink
              name="Table names &raquo; lowercase"
              onClick={() => CaseService.transform("tables", "lowercase")}
            />
            <PanelLink
              name="Table names &raquo; camelCase"
              onClick={() => CaseService.transform("tables", "camelCase")}
            />
            <PanelLink
              name="Table names &raquo; TitleCase"
              onClick={() => CaseService.transform("tables", "TitleCase")}
            />
            <PanelLink
              name="Table names &raquo; UPPERCASE"
              onClick={() => CaseService.transform("tables", "UPPERCASE")}
            />
            <PanelLink
              name="Column names &raquo; lowercase"
              onClick={() => CaseService.transform("columns", "lowercase")}
            />
            <PanelLink
              name="Column names &raquo; camelCase"
              onClick={() => CaseService.transform("columns", "camelCase")}
            />
            <PanelLink
              name="Column names &raquo; TitleCase"
              onClick={() => CaseService.transform("columns", "TitleCase")}
            />
            <PanelLink
              name="Column names &raquo; UPPERCASE"
              onClick={() => CaseService.transform("columns", "UPPERCASE")}
            />
            <PanelLink
              name="Detect table name convention"
              onClick={() => {
                console.log(AnalyseService.detectTableCase());
                console.log(AnalyseService.detectTableNumber());
              }}
            />
            <PanelLink
              name="Detect column name convention"
              onClick={() => {
                console.log(AnalyseService.detectColumnCase());
              }}
            />
          </Panel>

          <Panel name="All Actions">
            <PanelLink
              name="New Project"
              onClick={() => ProjectService.create()}
            />
            <PanelLink
              name="Load Project from localstorage"
              onClick={() => parent.loadProject()}
            />
            <PanelLink
              name="Save Project to localstorage"
              onClick={() => parent.saveProject()}
            />
            <PanelLink
              name="Save Project to database"
              onClick={() => ProjectService.save()}
            />
            <PanelLink
              name="Add New Table"
              onClick={() => ProjectService.createNewTable()}
            />
            <PanelLink
              name="Add New Connection"
              onClick={() => ProjectService.createNewConnection()}
            />
            <PanelLink name="Undo" onClick={() => HistoryService.undo()} />
            <PanelLink name="Redo" onClick={() => HistoryService.redo()} />
            <PanelLink
              name="Auto Arrange"
              onClick={() => ArrangeService.auto()}
            />
          </Panel>
        </Column>

        {parent.state.test && (
          <Column className="Panels2">
            <Panel name="Test" expanded>
              <PanelLink
                name="Toggle again"
                onClick={() => parent.toggleTest()}
              />
            </Panel>
          </Column>
        )}
      </>
    );
  }
}
