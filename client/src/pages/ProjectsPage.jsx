/*eslint-disable */

import React from "react";

import { Api, Keyboard, Store } from "core";

// ui
import { Button, ProjectCard, Column, Row, Scrollable } from "ui";

// components
import { Layer, Stage, StatusBar, Table, ToolBar } from "components";

// services
import { HistoryService, ProjectService } from "services";

/*eslint-enable */

export class ProjectsPage extends React.Component {
  state = {};

  constructor(props) {
    super(props);
    Store.register(this, "projectspage");
  }

  componentDidMount() {
    //
  }

  componentWillUnmount() {
    //
  }

  listProjects = e => {
    Api.get("/projects").then(response => {
      Store.set("app.projects", response.data);
    });
  };

  listMyProjects = e => {
    Api.get("/myprojects").then(response => {
      Store.set("app.projects", response.data);
    });
  };

  render() {
    const { projects } = Store.get("app");

    return (
      <Column>
        <Row style={{ background: "#444", maxHeight: "51px" }}>
          <Button onClick={this.listProjects}>Public Projects</Button>
          <Button onClick={this.listMyProjects}>My Projects</Button>
        </Row>
        <Row style={{ flexWrap: "wrap" }}>
          <Scrollable>
            {projects.map(proj => (
              <ProjectCard key={proj.id} project={proj} />
            ))}
          </Scrollable>
        </Row>
      </Column>
    );
  }
}
