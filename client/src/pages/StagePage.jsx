import React from "react";

import { Storage, Store, Page } from "core";

// ui
import { Column, Panel, PanelLink } from "ui";

// components
import {
  Connection,
  Layer,
  Modal,
  Stage,
  StatusBar,
  Table,
  ToolBar
} from "components";

// services
import { HistoryService } from "services";

import { StagePagePanels } from "./StagePagePanels";

export class StagePage extends React.Component {
  state = {};

  constructor(props) {
    super(props);
    Store.register(this, "stagepage");
  }

  componentDidMount() {
    //
  }

  componentWillUnmount() {
    //
  }

  // *** ACTIONS ***

  loadProject = event => {
    const project = Storage.load("project");
    if (project) {
      console.log("Project loaded from localstorage", project);
      // Store.set("app.project", project);
      Store.app = { project };
      HistoryService.add(project);
    }
  };

  saveProject = event => {
    const { project } = Store.get("app");
    Storage.save("project", project);
    console.log("Project saved", project);
  };

  toggleTest() {
    this.setState({ test: !this.state.test });
    console.log(this.state.test);
  }

  // *** EVENTS ***

  onStageSelect = model => {};

  onStageChange = model => {
    this.setState({ stage: model });
  };

  onStageDone = model => {
    this.setState({ stage: model });
  };

  onTableSelect = index => model => {};

  onTableChange = index => model => {
    const { project } = Store.get("app");
    project.schema.tables[index] = model;
    Store.set("app.project", project);
  };

  onTableDone = index => model => {
    const { project } = Store.get("app");
    project.schema.tables[index] = model;
    Store.set("app.project", project);
    HistoryService.add(project);
  };

  confirmed = (e, modal) => {
    modal.close();
    Page.open("import");
    return;
  };

  render() {
    const { project, stage } = Store.get("app");
    const { schema } = project;
    // console.log("bla", Object.getOwnPropertyDescriptors(Store.app));
    // TEST connections
    if (schema.tables.length > 2) {
      const p1 = { ...schema.tables[0].position };
      const p2 = { ...schema.tables[1].position };

      if (p1.x + p1.width <= p2.x) {
        console.log(1);

        p1.x = p1.x + p1.width;
      } else if (p1.x <= p2.x) {
        console.log(2);

        p1.x = p1.x + p1.width;
        p2.x = p2.x + p2.width;
      } else if (p2.x <= p1.x) {
        console.log(3);
        p2.x = p2.x + p2.width;
      } else if (p2.x + p2.width <= p1.x) {
        console.log(4);
        p2.x = p2.x + p2.width;
      }
      schema.connections[0] = {
        start: p1,
        end: p2
      };
      // console.log(p1.x, p1.y, p2.x, p2.y);
    }
    return (
      <>
        <Modal type="Confirm" onOK={this.confirmed}>
          Are you sure?
        </Modal>
        <Column className="Panels">
          <StagePagePanels parent={this} />
        </Column>

        {this.state.test && (
          <Column className="Panels2">
            <Panel name="Test" expanded>
              <PanelLink
                name="Toggle again"
                onClick={() => this.toggleTest()}
              />
            </Panel>
          </Column>
        )}
        <Column>
          <Stage stage={stage}>
            <Layer
              stage={stage}
              onChange={this.onStageChange}
              onDone={this.onStageDone}
            >
              {schema.tables.map((table, index) => (
                <Table
                  key={`t${index}`}
                  table={table}
                  index={index}
                  onChange={this.onTableChange(index)}
                  onDone={this.onTableDone(index)}
                />
              ))}
              <svg width="1000" height="1000">
                {schema.connections.map((connection, index) => (
                  <Connection key={`c${index}`} connection={connection} />
                ))}
              </svg>
              <div className="drawing-layer" />
            </Layer>
          </Stage>
          <ToolBar />
          <StatusBar />
        </Column>
      </>
    );
  }
}
