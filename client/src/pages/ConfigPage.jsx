/*eslint-disable */

import React from "react";

import { Api, Keyboard, Store } from "core";

// ui
import { Layout, Column, Panel, PanelLink } from "ui";

// components
import { Layer, Stage, StatusBar, Table, ToolBar } from "components";

// services
import { HistoryService, ProjectService } from "services";

/*eslint-enable */

export class ConfigPage extends React.Component {
  state = {};

  constructor(props) {
    super(props);
    Store.register(this, "configpage");
  }

  componentDidMount() {
    //
  }

  componentWillUnmount() {
    //
  }

  render() {
    return (
      <Column>
        <h1>Config Page</h1>
      </Column>
    );
  }
}
