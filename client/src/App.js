import React from "react";
import "App.scss";

import { Api, Key, Storage, Store } from "core";

// models
// import Model from "models";

// constants
// import { keyCodes } from "constants";

// ui
import { Layout, Menu } from "ui";

// components

// services
import { HistoryService, CaseService } from "services";

// pages
import { AccountPage } from "pages/AccountPage";
import { ConfigPage } from "pages/ConfigPage";
import { ExportPage } from "pages/ExportPage";
import { ImportPage } from "pages/ImportPage";
import { ProjectsPage } from "pages/ProjectsPage";
import { StagePage } from "pages/StagePage";
import { TestPage } from "pages/TestPage";
import { NotFoundPage } from "pages/NotFoundPage";

// data
import { emptyProject } from "mocks";

// keyboard events
Key.log();

const registeredPages = {
  ProjectsPage,
  StagePage,
  ImportPage,
  ExportPage,
  ConfigPage,
  AccountPage,
  TestPage,
  NotFoundPage
};

class App extends React.Component {
  state = {
    selected: [],
    stage: {
      zoom: 1,
      position: {
        x: 0,
        y: 0
      }
    },
    mode: "SIMPLE",
    project: emptyProject,
    myprojects: [],
    projects: [],
    test: false,
    page: "stage"
  };

  constructor(props) {
    super(props);
    Store.register(this, "app");
  }

  componentDidMount() {
    /*
    // load project mock from json file
    Api.get("http://localhost:3000/data/project.json").then(response => {
      const project = response.data;
      this.setState({ project });
      HistoryService.add(project);
    });
    */
    // load last state
    this.loadProject();
    // get list of projects saved in db
    Api.get("/myprojects").then(response => {
      Store.set("app.projects", response.data);
    });
  }

  loadProject = event => {
    const project = Storage.load("project");
    if (project) {
      console.log("Project loaded", project);
      this.setState({ project });
      HistoryService.add(project);
    }
  };

  render() {
    const { page } = this.state;

    // draft router
    const name = CaseService.titleCase(page) + "Page";
    const PageComponent = registeredPages[name];

    return (
      <Layout>
        <Menu />
        {registeredPages[name] ? <PageComponent /> : <NotFoundPage />}
      </Layout>
    );
  }
}

export default App;
