const primary = {
  name: "id",
  type: "INT",
  size: 14,
  unsigned: true,
  nullable: false,
  autoincrement: true
};
const name = {
  name: "name",
  type: "VARCHAR",
  size: 100,
  unsigned: false,
  nullable: true
};
const body = {
  name: "name",
  type: "TEXT",
  unsigned: false,
  nullable: false,
  defaults: ""
};

export const mockedTable = {
  type: "Table",
  name: "Books",
  position: {
    x: 0,
    y: 0
  },
  columns: [primary, name, body]
};

export const mockedConnection = {
  position: {
    x: 0,
    y: 0
  },
  start: {
    x: 0,
    y: 0
  },
  end: {
    x: 100,
    y: 100
  }
};

//

const table1 = mockedTable;
table1.name = "users";
const table2 = mockedTable;
table2.name = "books";
const table3 = mockedTable;
table3.name = "authors";

const con1 = mockedConnection;
con1.name = "t1-t2";

export const mockedProject = {
  name: "My project",
  lastSave: "20190531",
  code: "Person:",
  viewport: {
    x: 0,
    y: 0
  },
  schema: {
    tables: [table1, table2, table3],
    connections: [con1],
    groups: []
  }
};

export const emptyProject = {
  name: "Empty project",
  lastSave: "20190806",
  code: "Person:",
  viewport: {
    x: 0,
    y: 0
  },
  schema: {
    tables: [],
    connections: [],
    groups: []
  }
};
