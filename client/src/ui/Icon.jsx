import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCog,
  faFileAlt,
  faFileUpload,
  faFileDownload,
  faCoffee,
  faArrowAltCircleLeft,
  faArrowAltCircleRight,
  faUndoAlt,
  faRedoAlt,
  faVial,
  faThList,
  faThLarge,
  faUser,
  faUserAlt,
  faSquare,
  faCheckSquare,
  faSitemap
} from "@fortawesome/free-solid-svg-icons";

export const faIcons = {
  coffee: faCoffee,
  "file-alt": faFileAlt,
  import: faFileUpload,
  export: faFileDownload,
  "arrow-alt-cicle-left": faArrowAltCircleLeft,
  "arrow-alt-cicle-right": faArrowAltCircleRight,
  cog: faCog,
  undo: faUndoAlt,
  redo: faRedoAlt,
  vial: faVial,
  "th-list": faThList,
  "th-large": faThLarge,
  user: faUser,
  userAlt: faUserAlt,
  square: faSquare,
  "check-square": faCheckSquare,
  sitemap: faSitemap
};

export const faSizes = [
  "xs",
  "sm",
  "lg",
  "2x",
  "3x",
  "4x",
  "5x",
  "6x",
  "7x",
  "8x",
  "9x",
  "10x"
];

export class Icon extends React.PureComponent {
  render() {
    const { fa, size, style, svg } = this.props;

    const props = {};
    if (fa) {
      props.icon = faIcons[fa];
    }
    if (svg) {
      props.icon = svg;
    }
    if (faSizes.includes(size)) {
      props.size = size;
    }
    if (style) {
      props.style = style;
    }

    props.className = "Icon";

    return <FontAwesomeIcon {...props} />;
  }
}
