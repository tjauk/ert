import React from "react";
import "./Row.scss";

export class Row extends React.PureComponent {
  render() {
    const classNames = ["Row", this.props.className || null];
    return (
      <div {...this.props} className={classNames.join(" ")}>
        {this.props.children}
      </div>
    );
  }
}
