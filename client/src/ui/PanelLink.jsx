import React from "react";
import "./PanelLink.scss";

export class PanelLink extends React.PureComponent {
  render() {
    const { name, onClick } = this.props;
    return (
      <div
        className="PanelLink"
        onClick={onClick}
      >
        <span>{name}</span>
      </div>
    );
  }
}
