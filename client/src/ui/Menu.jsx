import React from "react";
import { Page } from "core";

// ui
import { Column, MenuOption } from "ui";

import "./Menu.scss";

export class Menu extends React.PureComponent {
  render() {
    return (
      <Column className="MenuOptions">
        <MenuOption
          name="Project"
          onClick={() => Page.open("stage")}
          tooltip="Current project"
          icon="th-large"
        />
        <MenuOption
          name="Projects"
          onClick={() => Page.open("projects")}
          tooltip="Projects"
          icon="file-alt"
        />
        <MenuOption
          name="Import"
          onClick={() => Page.open("import")}
          tooltip="Import"
          icon="import"
        />
        <MenuOption
          name="Export"
          onClick={() => Page.open("export")}
          tooltip="Export"
          icon="export"
        />
        <MenuOption
          name="Settings"
          onClick={() => Page.open("config")}
          tooltip="Settings"
          icon="cog"
        />
        <MenuOption
          name="Account"
          onClick={() => Page.open("account")}
          tooltip="Account"
          icon="user"
        />
        <MenuOption
          name="Test"
          onClick={() => Page.open("test")}
          tooltip="Test"
          icon="vial"
        />
      </Column>
    );
  }
}
