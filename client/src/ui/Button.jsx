import React from "react";
import "./Button.scss";

export class Button extends React.PureComponent {
  render() {
    const { className, primary } = this.props;
    const classNames = ["Button", className || null];
    if (primary) {
      classNames.push("primary");
    }
    return (
      <button {...this.props} className={classNames.join(" ")}>
        {this.props.children}
      </button>
    );
  }
}
