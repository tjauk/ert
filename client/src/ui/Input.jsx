import React from "react";
import "./Input.scss";

export class Input extends React.PureComponent {
  render() {
    const { type, className } = this.props;
    let classNames = ["Input", className || null];

    return (
      <input
        {...this.props}
        type={type || "text"}
        className={classNames.join(" ")}
      />
    );
  }
}
