import React from "react";
import "./ProjectCard.scss";
import { Button, Card } from "ui";
import { ProjectService } from "services";
import { Page } from "core";

export class ProjectCard extends React.PureComponent {
  onClick = () => {};

  load = event => {
    const { project } = this.props;
    ProjectService.load(project.id).then(project => {
      Page.open("stage");
    });
  };

  preview = event => {
    const { project } = this.props;
    ProjectService.preview(project.id).then(project => {
      console.log(project);
    });
  };

  delete = event => {
    const { project } = this.props;
    if (window.confirm("Are you sure")) {
      ProjectService.delete(project.id).then(() => {
        alert("Deleted project");
      });
    }
  };

  render() {
    const { project } = this.props;

    return (
      <Card className="ProjectCard">
        <h2>{project.name}</h2>
        <p>Description</p>
        <div className="info">{project.schema.length}</div>
        <div className="actions">
          <Button onClick={this.load}>Load</Button>
          <Button onClick={this.preview}>Preview</Button>
          {!!project.private && <Button onClick={this.delete}>Delete</Button>}
        </div>
      </Card>
    );
  }
}
