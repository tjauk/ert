import React from "react";
import "./Layout.scss";

export class Layout extends React.PureComponent {
  render() {
    return <div className="Layout">{this.props.children}</div>;
  }
}
