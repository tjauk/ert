import React from "react";
import "./Card.scss";

export class Card extends React.PureComponent {
  render() {
    const classNames = ["Card", this.props.className || null];

    return (
      <div className={classNames.join(" ")}>
        <div className="inner">{this.props.children}</div>
      </div>
    );
  }
}
