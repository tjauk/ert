import React from "react";
import "./Select.scss";

export class Select extends React.PureComponent {
  normalizeOptions(options) {
    if (Array.isArray(options) && options.length > 0) {
      if (typeof options[0] === "object") {
        return options;
      }
      return options.map(val => {
        return { value: val, label: val };
      });
    } else if (typeof options === "string") {
      return options.split(",").map(val => {
        return { value: val, label: val };
      });
    }
    return [];
  }

  render() {
    const { options, style, value, children, onChange, onClick } = this.props;
    const classNames = ["Select", this.props.className || null];
    const normalizedOptions = this.normalizeOptions(options);
    return (
      <select
        value={value}
        onChange={onChange}
        onClick={onClick}
        onMouseUp={e => {
          e.stopPropagation();
        }}
        onMouseDown={e => {
          e.stopPropagation();
        }}
        className={classNames.join(" ")}
        style={style}
      >
        {!children &&
          normalizedOptions.map((opt, index) => {
            return (
              <option key={index} value={opt.value}>
                {opt.label}
              </option>
            );
          })}
        {children}
      </select>
    );
  }
}
