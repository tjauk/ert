import React from 'react';
import { Icon } from './Icon';
import './MenuOption.scss';

export class MenuOption extends React.PureComponent {
  // name
  // tooltip
  // icon
  // action
  render() {
    const { name, icon, tooltip, onClick } = this.props;
    return (
      <div name={name} className="MenuOption" onClick={onClick} alt={tooltip}>
        <Icon fa={icon} size="2x" />
      </div>
    );
  }
}