import React from "react";
import "./Column.scss";

export class Column extends React.PureComponent {
  render() {
    const classNames = ["Column", this.props.className || null];

    return (
      <div {...this.props} className={classNames.join(" ")}>
        {this.props.children}
      </div>
    );
  }
}
