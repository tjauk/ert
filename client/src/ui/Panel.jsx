import React from "react";
import "./Panel.scss";
import { Store } from "core";
import { Scrollable } from "ui";

export class Panel extends React.PureComponent {
  initialState = {
    expanded: false
  };
  constructor(props) {
    super(props);
    if (props.expanded) {
      this.initialState.expanded = props.expanded;
    }
    Store.register(this, "_panel_" + props.name, this.initialState);
  }

  onClick = () => {
    this.setState({ expanded: !this.state.expanded });
  };

  render() {
    const { expanded } = this.state;
    const classNames = [
      "Panel",
      this.props.className || null,
      expanded ? "expanded" : null
    ];

    return (
      <div className={classNames.join(" ")}>
        <div className="Title" onClick={this.onClick}>
          {this.props.name}
        </div>
        <div className="Content">
          <Scrollable>{this.props.children}</Scrollable>
        </div>
      </div>
    );
  }
}
