import React from "react";
import "./Scrollable.scss";

/*
-webkit-scrollbar consists of seven different pseudo-elements, and together comprise a full scrollbar UI element:

::-webkit-scrollbar the background of the bar itself.
::-webkit-scrollbar-button the directional buttons on the scrollbar.
::-webkit-scrollbar-track the empty space “below” the progress bar.
::-webkit-scrollbar-track-piece the top-most layer of the the progress bar not covered by the thumb.
::-webkit-scrollbar-thumb the draggable scrolling element resizes depending on the size of the scrollable element.
::-webkit-scrollbar-corner the bottom corner of the scrollable element, where two scrollbar meet.
::-webkit-resizer the draggable resizing handle that appears above the scrollbar-corner at the bottom corner of some elements.

Now that you are familiar with the terminologies, let's start!
 */
export class Scrollable extends React.PureComponent {
  state = {
    expanded: false
  };
  constructor(props) {
    super(props);
    if (props.expanded) {
      this.state.expanded = true;
    }
  }

  onClick = () => {
    this.setState({ expanded: !this.state.expanded });
  };

  render() {
    const classNames = [
      "Scrollable",
      this.props.className || null,
      this.state.expanded ? "expanded" : null
    ];

    return (
      <div className={classNames.join(" ")}>
        <div className="Scrollable-Content">{this.props.children}</div>
      </div>
    );
  }
}
