onKeyDown = e => {
	if (e.keyCode === keyCodes.TAB) {
		e.preventDefault();
		e.stopPropagation();
	}
	if (e.keyCode === keyCodes.BACKSPACE) {
	}
};

handleCodeChange = e => {
	e.preventDefault();
	e.stopPropagation();
	const { project } = this.state;
	// implement code parser and formater
	let mode = "table";
	let lines = e.target.value.split("\n");
	lines.forEach((line, index) => {
		line = line.trim();
		let prev = lines[index - 1] || "";
		prev = prev.trim();
		if (
			prev.substring(prev.length - 1, prev.length) === ":" ||
			mode === "tab"
		) {
			line = "\t" + line;
			lines[index] = line;
			mode = "tab";
		}
		if (prev === "") {
			mode = "table";
		}
	});
	project.code = lines.join("\n");
	console.log(project.code);
	this.setState({ project });
};

toggle(name) {
	const { expanded } = this.state;
	const index = expanded.indexOf(name);
	index > -1 ? expanded.splice(index, 1) : expanded.push(name);
	this.setState({ expanded });
}


function syntaxHighlight(json) {
	if (typeof json != "string") {
		json = JSON.stringify(json, undefined, 2);
	}
	json = json
		.replace(/&/g, "&amp;")
		.replace(/</g, "&lt;")
		.replace(/>/g, "&gt;");
	return json.replace(
		/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+-]?\d+)?)/g,
		function (match) {
			var cls = "number";
			if (/^"/.test(match)) {
				if (/:$/.test(match)) {
					cls = "key";
				} else {
					cls = "string";
				}
			} else if (/true|false/.test(match)) {
				cls = "boolean";
			} else if (/null/.test(match)) {
				cls = "null";
			}
			return '<span class="' + cls + '">' + match + "</span>";
		}
	);
}