export const draggable = {
    draggable: true,
    onDragStart: () => (event) => {
        event.stopPropagation();
        this.setState({
            isDragging: true
        });
    },
    onDragEnd: () => (event) => {
        this.setState({
            isDragging: false,
            x: event.target.x(),
            y: event.target.y()
        });
    }
};