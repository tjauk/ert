import { History, Store } from "core";

export default class HistoryService {
  static add(project) {
    History.addToHistory(project);
  }
  static undo() {
    const current = Store.get("app.project");
    const next = History.undoProject(current);
    Store.set("app.project", next);
  }
  static redo() {
    const current = Store.get("app.project");
    const next = History.redoProject(current);
    Store.set("app.project", next);
  }
}
