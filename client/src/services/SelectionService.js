import { Key, Store } from "core";

export default class SelectionService {
  static add(index) {
    const app = Store.get("app");
    if (!Key.CTRL()) {
      app.project.schema.tables.map(table => (table.selected = false));
      app.selected = [];
    }

    app.project.schema.tables[index].selected = true;
    app.selected = app.project.schema.tables.filter(table => table.selected);
    Store.set("app", app);
  }

  static remove(index) {
    const app = Store.get("app");
    if (!Key.CTRL()) {
      app.project.schema.tables.map(table => (table.selected = false));
      app.selected = [];
    }
    app.project.schema.tables[index].selected = false;
    app.selected = app.project.schema.tables.filter(table => table.selected);
    Store.set("app", app);
  }

  static toggle(index) {
    const app = Store.get("app");
    if (!Key.CTRL()) {
      app.project.schema.tables.map(table => (table.selected = false));
      app.selected = [];
    }
    app.project.schema.tables[index].selected = !app.project.schema.tables[
      index
    ].selected;
    app.selected = app.project.schema.tables.filter(table => table.selected);
    Store.set("app", app);
  }

  static clear() {
    const app = Store.get("app");
    app.project.schema.tables.map(table => (table.selected = false));
    app.selected = [];
    Store.set("app", app);
  }

  static total() {
    return Store.get("app.selected").length;
  }

  static isSingle() {
    return Store.get("app.selected").length < 2 ? true : false;
  }

  static isMultiple() {
    return Store.get("app.selected").length > 1 ? true : false;
  }
}
