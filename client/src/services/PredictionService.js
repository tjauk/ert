import { Api, Store } from "core";

const BASEURL = "http://localhost:8000/api";

export default class PredictionService {
  static columnNames() {
    Api.post(BASEURL + "/predict").then(response => {
      console.log(response.data);
      Store.set("app.prediction", response.data);
    });
  }
}
