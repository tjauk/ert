import { Api, Store } from "core";

export default class SuggestionService {
  static table() {
    const data = {};
    Api.post("http://localhost:8000/api/suggest/table", data).then(response => {
      Store.set("app.suggestions", response.data);
    });
  }
  static field() {
    const data = {};
    Api.post("http://localhost:8000/api/suggest/field", data).then(response => {
      Store.set("app.suggestions", response.data);
    });
  }
}
