import { Api, Store } from "core";

const BASEURL = "http://localhost:8000/api";

export default class ImportService {
  static text(sql) {
    const data = { sql };
    return Api.post(BASEURL + "/import/text", data).then(response => {
      console.log(data, response);
      const { success, schema } = response.data;
      let table_names = [];
      if (success) {
        const { project } = Store.get("app");
        schema.tables.forEach(table => {
          table_names.push(table.name);
          project.schema.tables.push(table);
        });
        Store.set("app.project", project);
        return project.schema;
      }
      return [];
    });
  }
}
