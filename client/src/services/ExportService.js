import { Api, Store } from "core";

export default class ExportService {
  static temp() {
    const data = {};
    Api.post("http://localhost:8000/api/suggest/table", data).then(response => {
      Store.set("app.suggestions", response.data);
    });
  }
}
