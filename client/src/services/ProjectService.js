import { Api, History, Store } from "core";
import { HistoryService } from "services";

// data
import { mockedProject } from "mocks";

export default class ProjectService {
  static list() {
    return Api.get("/projects").then(response => {
      Store.set("app.projects", response.data);
    });
  }

  static create() {
    const newProject = mockedProject;
    newProject.schema.tables = [];
    newProject.schema.connections = [];
    Store.set("app.project", newProject);
    History.addToHistory(newProject);
  }

  static load(id) {
    return Api.get("/projects/" + id).then(response => {
      const project = response.data;
      if (project.schema && project.schema.tables) {
        if (!project.schema.connections) {
          project.schema.connections = [];
        }
        Store.set("app.project", project);
        HistoryService.add(project);
        // support old schema format
      } else if (project.schema.schema && project.schema.schema.tables) {
        if (!project.schema.schema.connections) {
          project.schema.schema.connections = [];
        }
        Store.set("app.project", project.schema);
        HistoryService.add(project.schema);
        console.warn("Loaded using old schema format", project);
      } else {
        console.warn("Invalid project format", project);
      }
      return project;
    });
  }

  static preview(id) {
    return Api.get("/projects/" + id).then(response => {
      const project = response.data;
      Store.set("app.preview", project);
      return project;
    });
  }

  static save() {
    const app = Store.get("app");
    const { project } = app;
    if (project.id && project.id > 0) {
      // update
      // const Project = { name: "Updated project", schema: project.schema };
      return Api.put("/projects/" + project.id, project);
    } else {
      // insert
      project.name = "My new project";
      return Api.post("/projects", project);
    }
  }

  static saveAs() {}

  static delete(id) {
    return Api.delete("/projects/" + id, { id });
  }

  // TABLE METHODS
  static createNewTable = event => {
    const { project } = Store.get("app");

    const table = {
      type: "Table",
      name: "Table " + project.schema.tables.length,
      position: {
        x: 50,
        y: 50
      },
      columns: []
    };
    project.schema.tables.push(table);

    Store.set("app.project", project);
    HistoryService.add(project);
  };

  // CONNECTION METHODS
  static createNewConnection = event => {
    const { project } = Store.get("app");

    const connection = {
      position: {
        x: 50,
        y: 50
      },
      start: {
        x: -100,
        y: -100
      },
      end: {
        x: 200,
        y: 200
      }
    };
    project.schema.connections.push(connection);
    console.log(project.schema.connections);

    Store.set("app.project", project);
    HistoryService.add(project);
  };

  // GROUP METHODS
}
