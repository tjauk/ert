import { Store } from "core";
import { HistoryService } from "services";

import {
  camelCase,
  kebabCase,
  lowerCase,
  snakeCase,
  upperCase
} from "lodash/fp";

import pluralize from "pluralize";

// schema.tables[].columns[].name
export default class CaseService {
  static getSchema() {
    return Store.get("app.project.schema");
  }

  static setSchema(schema) {
    const { project } = Store.get("app");
    project.schema = schema;
    Store.set("app.project", project);
    HistoryService.add(project);
  }

  static transform(scope, method) {
    let schema = this.getSchema();
    schema.tables.map(table => {
      if (scope === "tables" || scope === "schema") {
        table.name = this.applyMethod(table.name, method);
      }
      if (scope === "columns" || scope === "schema") {
        table.columns.map(column => {
          column.name = this.applyMethod(column.name, method);
          return column;
        });
      }
      return table;
    });
    this.setSchema(schema);
  }

  static applyMethod(string, method) {
    let out = string;
    if ("camelCase" === method) {
      out = camelCase(string);
    } else if ("kebabCase" === method) {
      out = kebabCase(string);
      out = out.split("-").join("_");
    } else if ("lowercase" === method) {
      out = lowerCase(string);
      out = out.split(" ").join("_");
    } else if ("snakeCase" === method) {
      out = snakeCase(string);
    } else if ("TitleCase" === method) {
      out = this.titleCase(string);
    } else if ("UPPERCASE" === method) {
      out = upperCase(string);
      out = out.split(" ").join("_");
    } else if ("singular" === method) {
      out = out.split("_").join(" ");
      out = pluralize.singular(out);
      out = out.split(" ").join("_");
    } else if ("plural" === method) {
      out = out.split("_").join(" ");
      out = pluralize.plural(out);
      out = out.split(" ").join("_");
    }
    return out;
  }

  static detect(samples) {}

  //

  static titleCase(str) {
    str = camelCase(str);
    return str.charAt(0).toUpperCase() + str.substr(1);
  }
}
