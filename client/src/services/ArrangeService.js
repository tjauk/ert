import { History, Store } from "core";

export default class ArrangeService {
  static auto() {
    const app = Store.get("app");

    const { project, stage } = app;

    const offset = {
      x: 100,
      y: 100
    };
    stage.position.x = 0;
    stage.position.y = 0;

    const total = project.schema.tables.length;
    let cols = Math.ceil(Math.sqrt(total) * 1.2);
    let rows = Math.ceil(Math.sqrt(total) / 1.2);
    let x = 0;
    let y = 0;
    let w = 175 + 25;
    let h = 250 + 25;
    let matrix = [];
    for (let row = 0; row < rows; row++) {
      for (let col = 0; col < cols; col++) {
        matrix.push([col, row]);
      }
    }

    let index = 0;
    project.schema.tables.map(table => {
      let mt = matrix[index];
      x = mt[0] * w + offset.x;
      y = mt[1] * h + offset.y;
      table.position = { x, y };
      index++;
      return table;
    });

    History.addToHistory(project);
    app.project = project;
    app.stage = stage;
    Store.set("app", app);
  }
}
