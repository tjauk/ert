import { Store } from "core";
import { CaseService } from "services";

export default class AnalyseService {
  // analyse table names and get word case convention in use
  static detectTableCase() {
    const methods = ["lowercase", "camelCase", "TitleCase", "UPPERCASE"];
    const tables = Store.get("app.project.schema.tables");
    const tableNames = tables.map(table => table.name);
    const found = {};
    methods.forEach(method => {
      found[method] = 0;
    });
    tableNames.forEach(name => {
      methods.forEach(method => {
        if (name === CaseService.applyMethod(name, method)) {
          found[method]++;
        }
      });
    });
    const method = Object.keys(found).reduce((a, b) =>
      found[a] > found[b] ? a : b
    );
    return {
      method,
      found
    };
  }

  // analyse table names and get number convention in use
  static detectTableNumber() {
    const methods = ["singular", "plural"];
    const tables = Store.get("app.project.schema.tables");
    const tableNames = tables.map(table => table.name);
    const found = {};
    methods.forEach(method => {
      found[method] = 0;
    });
    tableNames.forEach(name => {
      methods.forEach(method => {
        if (name === CaseService.applyMethod(name, method)) {
          found[method]++;
        }
      });
    });
    const method = Object.keys(found).reduce((a, b) =>
      found[a] > found[b] ? a : b
    );
    return {
      method,
      found
    };
  }

  // analyse column names and get word case convention in use
  static detectColumnCase() {
    const methods = ["lowercase", "camelCase", "TitleCase", "UPPERCASE"];
    const tables = Store.get("app.project.schema.tables");
    const found = {};
    methods.forEach(method => {
      found[method] = 0;
    });
    tables.forEach(table => {
      table.columns.forEach(column => {
        methods.forEach(method => {
          if (column.name === CaseService.applyMethod(column.name, method)) {
            found[method]++;
          }
        });
      });
    });
    const method = Object.keys(found).reduce((a, b) =>
      found[a] > found[b] ? a : b
    );
    return {
      method,
      found
    };
  }
}
