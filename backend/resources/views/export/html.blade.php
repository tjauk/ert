<html>
    <head>
        <title>Export to HTML template</title>
        <meta content="utf-8">
    </head>
    <body>
        @foreach ($project->schema['tables'] as $table)
        <table style="border:solid 1px #f00;min-width:400px">
            <thead style="background:#eee">
                <tr>
                    <td colspan="4">{{ $table['name'] }}</td>
                </tr>
            </thead>
            <tbody>
                @foreach ($table['columns'] as $column)
                <tr>
                    <td>{{ $column['name'] }}</td>
                    <td>{{ isset($column['type']) ? $column['type'] : 'none' }}</td>
                    <td>Size</td>
                    <td>Props</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @endforeach
    </body>
</html>