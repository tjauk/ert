-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.1.0.5464
-- --------------------------------------------------------
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- Dumping structure for table chipoteka_app.agreements
CREATE TABLE IF NOT EXISTS `agreements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'newsletter',
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `agreed` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `agreed_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 11092 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table chipoteka_app.audits
CREATE TABLE IF NOT EXISTS `audits` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `code` text COLLATE utf8_unicode_ci,
  `message` text COLLATE utf8_unicode_ci,
  `data` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 63623 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table chipoteka_app.b2b_korisnik
CREATE TABLE IF NOT EXISTS `b2b_korisnik` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tm` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `partner_naziv` varchar(255) NOT NULL DEFAULT '',
  `partner` varchar(255) NOT NULL DEFAULT '',
  `partner_uid` varchar(50) NOT NULL DEFAULT '',
  `partner_oib` varchar(30) NOT NULL DEFAULT '0',
  `username` varchar(255) NOT NULL DEFAULT '',
  `passw` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `user_group` varchar(40) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 149 DEFAULT CHARSET = utf8 ROW_FORMAT = COMPACT;
-- Data exporting was unselected.
-- Dumping structure for table chipoteka_app.brands
CREATE TABLE IF NOT EXISTS `brands` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `badge` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `web` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `robna_marka_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `robna_marka` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `robna_marka_naziv` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `robna_marka_b2b` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_products` int(10) unsigned NOT NULL DEFAULT '0',
  `search_index` longtext COLLATE utf8_unicode_ci NOT NULL,
  `elements` longtext COLLATE utf8_unicode_ci,
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `meta_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `meta_description` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 412 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table chipoteka_app.countries
CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `code` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `pri` smallint(6) NOT NULL DEFAULT '0',
  `iso3166` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `iso3166a3` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `drzava_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `drzava_b2b` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shippable` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 253 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table chipoteka_app.credit_cards
CREATE TABLE IF NOT EXISTS `credit_cards` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `rates` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 6 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table chipoteka_app.demos
CREATE TABLE IF NOT EXISTS `demos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `active` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE = MyISAM AUTO_INCREMENT = 2 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table chipoteka_app.documents
CREATE TABLE IF NOT EXISTS `documents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `uploaded_by` int(10) unsigned NOT NULL DEFAULT '0',
  `shared` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ext` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `size` int(10) unsigned NOT NULL DEFAULT '0',
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table chipoteka_app.event_logs
CREATE TABLE IF NOT EXISTS `event_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `info` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 300 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table chipoteka_app.home_listings
CREATE TABLE IF NOT EXISTS `home_listings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `color` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `icon` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tags` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `priority` int(10) unsigned NOT NULL DEFAULT '1',
  `active` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `or_tags` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `and_tags` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `side` tinyint(4) NOT NULL DEFAULT '0',
  `photos` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `b2b` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `product_codes` longtext COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `selling_action` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `published_at` datetime DEFAULT NULL,
  `expired_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 75 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table chipoteka_app.import_logs
CREATE TABLE IF NOT EXISTS `import_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `value` int(11) NOT NULL DEFAULT '0',
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'RUNNING',
  `duration` double(8, 2) unsigned DEFAULT NULL,
  `memory` double(8, 2) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 41331 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table chipoteka_app.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `migrations_migration_unique` (`migration`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table chipoteka_app.news
CREATE TABLE IF NOT EXISTS `news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `published_at` datetime DEFAULT NULL,
  `expired_at` datetime DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'draft',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Untitled',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `photos` text COLLATE utf8_unicode_ci NOT NULL,
  `summary` text COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `elements` longtext COLLATE utf8_unicode_ci,
  `tags` text COLLATE utf8_unicode_ci NOT NULL,
  `products` text COLLATE utf8_unicode_ci NOT NULL,
  `web_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `video_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `b2b` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `meta_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `meta_description` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 190 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table chipoteka_app.options
CREATE TABLE IF NOT EXISTS `options` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `options_name_unique` (`name`)
) ENGINE = InnoDB AUTO_INCREMENT = 7 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table chipoteka_app.orders
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `ordered_at` datetime DEFAULT NULL,
  `year` int(10) unsigned NOT NULL,
  `number` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SHOPPING',
  `luceed_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `items` longtext COLLATE utf8_unicode_ci,
  `shipment_method` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `payment_method` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `subtotal` double(8, 2) unsigned NOT NULL,
  `shipment_cost` double(8, 2) unsigned NOT NULL,
  `tax` double(8, 2) unsigned NOT NULL,
  `total` double(8, 2) unsigned NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `partner_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_subject_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'F',
  `billing_firstname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_lastname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_oib` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_zip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_country_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_mjesto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billing_drzava` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `same_address` tinyint(4) NOT NULL DEFAULT '1',
  `korisnik_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_subject_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'F',
  `shipping_firstname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_lastname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_oib` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_zip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_country_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_mjesto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_drzava` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `b2b` tinyint(4) NOT NULL DEFAULT '0',
  `credit_card_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `installments` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pg_response` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 1348591 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table chipoteka_app.pages
CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Untitled',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'draft',
  `published_at` datetime DEFAULT NULL,
  `category_id` int(10) unsigned DEFAULT NULL,
  `photos` text COLLATE utf8_unicode_ci NOT NULL,
  `summary` text COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `elements` longtext COLLATE utf8_unicode_ci,
  `tags` text COLLATE utf8_unicode_ci NOT NULL,
  `search_index` longtext COLLATE utf8_unicode_ci NOT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `meta_description` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 13 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table chipoteka_app.partners
CREATE TABLE IF NOT EXISTS `partners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `oib` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `plain` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `partner` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `partner_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table chipoteka_app.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `password_resets_email_unique` (`email`),
  UNIQUE KEY `password_resets_token_unique` (`token`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table chipoteka_app.products
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `summary` text COLLATE utf8_unicode_ci NOT NULL,
  `vpc` double(12, 4) unsigned DEFAULT NULL,
  `mpc` double(12, 4) unsigned DEFAULT NULL,
  `webshop_price` double(12, 4) unsigned DEFAULT NULL,
  `action_price` double(12, 4) unsigned DEFAULT NULL,
  `price` double(12, 4) unsigned DEFAULT NULL,
  `photos` text COLLATE utf8_unicode_ci NOT NULL,
  `sid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `artikl_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `artikl_b2b` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `artikl` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `naziv` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `barcode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kataloski_broj` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enabled` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'D',
  `usluga` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `opis` text COLLATE utf8_unicode_ci,
  `specifikacija` text COLLATE utf8_unicode_ci,
  `atributi` text COLLATE utf8_unicode_ci,
  `dokumenti` text COLLATE utf8_unicode_ci,
  `duzina` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sirina` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `visina` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `masa` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `volumen` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `naziv_kratki` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `upozorenje` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stopa_pdv` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supergrupa_artikla` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supergrupa_artikla_naziv` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nadgrupa_artikla` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nadgrupa_artikla_naziv` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `grupa_artikla_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `grupa_artikla` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `grupa_artikla_naziv` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `robna_marka_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `robna_marka` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `robna_marka_naziv` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `robna_marka_b2b` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jamstvo_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jamstvo_b2b` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jamstvo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jamstvo_naziv` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stanje_kol` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `raspolozivo_kol` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stanje` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `raspolozivo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `json` text COLLATE utf8_unicode_ci NOT NULL,
  `jsonhash` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `changed_at` datetime NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `title_lock` tinyint(4) NOT NULL DEFAULT '0',
  `description_lock` tinyint(4) NOT NULL DEFAULT '0',
  `photos_lock` tinyint(4) NOT NULL DEFAULT '0',
  `hidden_tags` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `active_tags` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `marketing_tags` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `is_new` tinyint(4) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `homepage` tinyint(4) NOT NULL DEFAULT '0',
  `happyday` tinyint(4) NOT NULL DEFAULT '0',
  `stores` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `content_density` text COLLATE utf8_unicode_ci NOT NULL,
  `content_boost` int(11) NOT NULL DEFAULT '0',
  `content_score` int(11) NOT NULL DEFAULT '0',
  `search_index` longtext COLLATE utf8_unicode_ci NOT NULL,
  `luceed_tags` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `video_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `all_tags` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `carted_at` datetime DEFAULT NULL,
  `verified_by` int(11) DEFAULT NULL,
  `verified_at` datetime DEFAULT NULL,
  `category_tags` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `type_tags` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `type_id` int(10) unsigned DEFAULT NULL,
  `eol` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `pa_id` int(11) DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `meta_description` text COLLATE utf8_unicode_ci,
  `custom_deliverable` int(11) NOT NULL DEFAULT '0',
  `zpc` double(8, 2) DEFAULT NULL,
  `zaba_price` double(8, 2) DEFAULT NULL,
  `description_html` text COLLATE utf8_unicode_ci,
  `meta_keywords` text COLLATE utf8_unicode_ci,
  `og_image` text COLLATE utf8_unicode_ci,
  `listing_tags` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `boja` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `boja_naziv` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `products_artikl_uid_unique` (`artikl_uid`)
) ENGINE = InnoDB AUTO_INCREMENT = 18615 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table chipoteka_app.product_attributes
CREATE TABLE IF NOT EXISTS `product_attributes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `atribut` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `atribut_uid` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `b2b_atribut` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `naziv` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `atribut_tip` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `aktivan` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `redoslijed` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vidljiv` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `vrijednost` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 37941 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table chipoteka_app.product_docs
CREATE TABLE IF NOT EXISTS `product_docs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `file_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `naziv` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `opis` text COLLATE utf8_unicode_ci,
  `md5` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `downloaded` tinyint(4) NOT NULL DEFAULT '0',
  `filepath` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seopath` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 36889 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table chipoteka_app.product_groups
CREATE TABLE IF NOT EXISTS `product_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `slugpath` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `level` int(10) unsigned NOT NULL,
  `cpath` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `parent` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `parent_id` int(10) unsigned NOT NULL,
  `grupa_artikla_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `grupa_artikla_b2b` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `grupa_artikla` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `naziv` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `barcode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enabled` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `redoslijed` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nadgrupa__grupa_artikla_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nadgrupa__grupa_artikla_b2b` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nadgrupa_artikla` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nadgrupa__naziv` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marker_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marker_b2b` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marker` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marker_naziv` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `atributi` text COLLATE utf8_unicode_ci NOT NULL,
  `json` text COLLATE utf8_unicode_ci NOT NULL,
  `jsonhash` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `changed_at` datetime NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `search_index` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 856 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table chipoteka_app.product_stats
CREATE TABLE IF NOT EXISTS `product_stats` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_id` int(10) unsigned DEFAULT NULL,
  `order_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE = MyISAM AUTO_INCREMENT = 7 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table chipoteka_app.product_viewed
CREATE TABLE IF NOT EXISTS `product_viewed` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `products` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 5484 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table chipoteka_app.promos
CREATE TABLE IF NOT EXISTS `promos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `selling_action_id` int(10) unsigned DEFAULT NULL,
  `trigger` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `public` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_id` int(10) unsigned DEFAULT NULL,
  `price` double(12, 4) DEFAULT NULL,
  `discount` double(12, 4) DEFAULT NULL,
  `free_shipping` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE = MyISAM AUTO_INCREMENT = 18 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table chipoteka_app.queues
CREATE TABLE IF NOT EXISTS `queues` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `fx` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `args` text COLLATE utf8_unicode_ci,
  `retry` int(11) NOT NULL DEFAULT '0',
  `err` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table chipoteka_app.search_logs
CREATE TABLE IF NOT EXISTS `search_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `day` date NOT NULL,
  `term` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `method` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'GET',
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `params` text COLLATE utf8_unicode_ci NOT NULL,
  `count` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 7513731 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table chipoteka_app.selling_actions
CREATE TABLE IF NOT EXISTS `selling_actions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `prodajna_akcija_uid` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `naziv` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pj_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pj_naziv` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `end_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start_time` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `end_time` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cet` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pet` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sub` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ned` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vpc_rabat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mpc_rabat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `json` text COLLATE utf8_unicode_ci,
  `jsonhash` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `changed_at` datetime DEFAULT NULL,
  `stavke` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 112 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table chipoteka_app.side_listings
CREATE TABLE IF NOT EXISTS `side_listings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `priority` int(10) unsigned NOT NULL DEFAULT '1',
  `active` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `photos` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `or_tags` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `and_tags` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `b2b` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `published_at` datetime DEFAULT NULL,
  `expired_at` datetime DEFAULT NULL,
  `selling_action` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 43 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table chipoteka_app.stocks
CREATE TABLE IF NOT EXISTS `stocks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `artikl_uid` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `skladiste_uid` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `stanje_kol` int(11) NOT NULL,
  `raspolozivo_kol` int(11) NOT NULL,
  `stanje` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `raspolozivo` char(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 39972 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table chipoteka_app.suppliers
CREATE TABLE IF NOT EXISTS `suppliers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sifra_dobavljac` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `naziv_dobavljaca` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `foreign` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `b2b_visible` tinyint(4) NOT NULL DEFAULT '1',
  `deliverable_days` int(10) unsigned NOT NULL DEFAULT '14',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 870 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table chipoteka_app.supplier_stocks
CREATE TABLE IF NOT EXISTS `supplier_stocks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sifra` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `naziv` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dobavljac_info` text COLLATE utf8_unicode_ci,
  `dobavljac_stanje` int(11) NOT NULL DEFAULT '0',
  `dobavljac_nc` int(11) NOT NULL DEFAULT '0',
  `dobavljac_valuta` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sifra_dobavljac` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `naziv_dobavljaca` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sifra_artikla` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `naziv_artikla` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `main` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 573 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table chipoteka_app.tags
CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `taxonomy_id` int(10) unsigned DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `parent_id` int(11) DEFAULT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `weight` double(8, 2) NOT NULL DEFAULT '1.00',
  `description` text COLLATE utf8_unicode_ci,
  `elements` longtext COLLATE utf8_unicode_ci,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `synonyms` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `parents` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `filter_tags` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `group_tags` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `and_tags` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `product_types` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `html` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 5162 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table chipoteka_app.taxonomies
CREATE TABLE IF NOT EXISTS `taxonomies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `sort_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'default',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 9 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table chipoteka_app.translations
CREATE TABLE IF NOT EXISTS `translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `translation_tag_id` int(10) unsigned NOT NULL,
  `lang` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en',
  `translation` text COLLATE utf8_unicode_ci NOT NULL,
  `auto` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 103 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table chipoteka_app.translation_tags
CREATE TABLE IF NOT EXISTS `translation_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `scope` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tag` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 103 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table chipoteka_app.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `usergroup` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `blocked` tinyint(3) unsigned DEFAULT NULL,
  `last_seen` datetime DEFAULT NULL,
  `partner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `partner_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `partner_oib` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `last_order_id` int(11) DEFAULT NULL,
  `secure_action` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `secure_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `token_expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 6503 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table chipoteka_app.user_data
CREATE TABLE IF NOT EXISTS `user_data` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci NOT NULL,
  `uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 4507 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table chipoteka_app.user_groups
CREATE TABLE IF NOT EXISTS `user_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `permissions` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 12 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table chipoteka_app.user_points
CREATE TABLE IF NOT EXISTS `user_points` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `points` int(11) NOT NULL,
  `reason` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 1596 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table chipoteka_app.user_servis
CREATE TABLE IF NOT EXISTS `user_servis` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `webcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `last_time` datetime DEFAULT NULL,
  `last_status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 39 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table chipoteka_app.user_socials
CREATE TABLE IF NOT EXISTS `user_socials` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `provider` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `identifier` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table chipoteka_app.user_throttle
CREATE TABLE IF NOT EXISTS `user_throttle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `ip_address` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `invalid_tries` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `data` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 2233 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table chipoteka_app.warehouses
CREATE TABLE IF NOT EXISTS `warehouses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `skladiste_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `skladiste` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `naziv` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pj_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pj` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pj_naziv` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adresa` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postanski_broj` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mjesto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefax` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefax_1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `e_mail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `json` text COLLATE utf8_unicode_ci NOT NULL,
  `jsonhash` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `changed_at` datetime NOT NULL,
  `short` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `take_stocks` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `for_webshop` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `b2b` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `map` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 99 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;