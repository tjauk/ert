-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.1.0.5464
-- --------------------------------------------------------
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- Dumping structure for table rent_app.audits
CREATE TABLE IF NOT EXISTS `audits` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `code` text COLLATE utf8_unicode_ci,
  `message` text COLLATE utf8_unicode_ci,
  `data` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 18021 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table rent_app.bookings
CREATE TABLE IF NOT EXISTS `bookings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `booked_by` int(10) unsigned NOT NULL,
  `from` datetime DEFAULT NULL,
  `to` datetime DEFAULT NULL,
  `vehicle_id` int(10) unsigned NOT NULL,
  `km_start` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `km_end` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `price` double(8, 2) NOT NULL,
  `client_id` int(10) unsigned DEFAULT NULL,
  `partner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `docs` text COLLATE utf8_unicode_ci NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `paid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `related_doc` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `partial_amount` double(8, 2) NOT NULL,
  `status` int(10) unsigned NOT NULL DEFAULT '0',
  `service_type` tinyint(4) NOT NULL DEFAULT '10',
  `contracted_km` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `partner_price` double(8, 2) NOT NULL,
  `our_provision` double(8, 2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 3247 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table rent_app.checklists
CREATE TABLE IF NOT EXISTS `checklists` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `check_date` date NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  `data` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 125 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table rent_app.contacts
CREATE TABLE IF NOT EXISTS `contacts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `last_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `company` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `company_long` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `oib` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `mb` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `address` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `address_2` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `zip` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `city` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `country_id` int(11) NOT NULL DEFAULT '52',
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `mobile` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `phone` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `fax` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `web` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `groups` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `contact_person` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `contact_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tags` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 2595 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table rent_app.contracts
CREATE TABLE IF NOT EXISTS `contracts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `data` text COLLATE utf8_unicode_ci NOT NULL,
  `pdf` text COLLATE utf8_unicode_ci NOT NULL,
  `booking_id` int(11) DEFAULT NULL,
  `vehicle_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `dated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 1672 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table rent_app.contract_templates
CREATE TABLE IF NOT EXISTS `contract_templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `template` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table rent_app.countries
CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `code` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `pri` smallint(6) NOT NULL DEFAULT '0',
  `iso3166` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `iso3166a3` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 253 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table rent_app.damages
CREATE TABLE IF NOT EXISTS `damages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `vehicle_id` int(11) NOT NULL DEFAULT '0',
  `vehicle_km` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `date` date DEFAULT NULL,
  `where` text COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `docs` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 61 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table rent_app.documents
CREATE TABLE IF NOT EXISTS `documents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `uploaded_by` int(10) unsigned NOT NULL DEFAULT '0',
  `shared` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ext` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `size` int(10) unsigned NOT NULL DEFAULT '0',
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 353 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table rent_app.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `migrations_migration_unique` (`migration`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table rent_app.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  UNIQUE KEY `password_resets_email_unique` (`email`),
  UNIQUE KEY `password_resets_token_unique` (`token`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table rent_app.reminders
CREATE TABLE IF NOT EXISTS `reminders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `docs` text COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `assigned_to` int(10) unsigned NOT NULL DEFAULT '0',
  `followers` text COLLATE utf8_unicode_ci NOT NULL,
  `done` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `done_at` datetime NOT NULL,
  `done_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table rent_app.tasks
CREATE TABLE IF NOT EXISTS `tasks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL,
  `assigned_to` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start` datetime NOT NULL,
  `due` datetime NOT NULL,
  `finish` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table rent_app.translations
CREATE TABLE IF NOT EXISTS `translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `translation_tag_id` int(10) unsigned NOT NULL,
  `lang` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en',
  `translation` text COLLATE utf8_unicode_ci NOT NULL,
  `auto` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 184 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table rent_app.translation_tags
CREATE TABLE IF NOT EXISTS `translation_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `scope` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tag` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 183 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table rent_app.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `usergroup` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `blocked` tinyint(3) unsigned DEFAULT NULL,
  `last_seen` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 8 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table rent_app.user_groups
CREATE TABLE IF NOT EXISTS `user_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `permissions` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 9 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table rent_app.user_throttle
CREATE TABLE IF NOT EXISTS `user_throttle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `ip_address` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `invalid_tries` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `data` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 148 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table rent_app.vehicles
CREATE TABLE IF NOT EXISTS `vehicles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `brand` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `model` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `partner` tinyint(4) NOT NULL DEFAULT '0',
  `year` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `color` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'White',
  `color_hex` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '#FFFFFF',
  `chassis_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `leasing` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `leasing_amount` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tire_size` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `pp_expires` date DEFAULT NULL,
  `casco_expires` date DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `licence_plate` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `licence_expires` date DEFAULT NULL,
  `ppa_expires` date DEFAULT NULL,
  `ao_expires` date DEFAULT NULL,
  `ao_house` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `kasko_expires` date DEFAULT NULL,
  `kasko_house` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `km` int(10) unsigned NOT NULL DEFAULT '0',
  `service_km` int(10) unsigned NOT NULL DEFAULT '50000',
  `docs` text COLLATE utf8_unicode_ci NOT NULL,
  `show_on_calendar` int(11) NOT NULL DEFAULT '1',
  `active` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `tire_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `leasing_stake` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 62 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table rent_app.vehicle_kms
CREATE TABLE IF NOT EXISTS `vehicle_kms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `vehicle_id` int(10) unsigned NOT NULL,
  `km` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 1676 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
-- Dumping structure for table rent_app.vehicle_services
CREATE TABLE IF NOT EXISTS `vehicle_services` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `vehicle_id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Redovan',
  `from` date DEFAULT NULL,
  `to` date DEFAULT NULL,
  `service_price` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `parts_price` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `docs` text COLLATE utf8_unicode_ci NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `vehicle_km` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `service_id` int(11) DEFAULT NULL,
  `service_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 882 DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;
-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;