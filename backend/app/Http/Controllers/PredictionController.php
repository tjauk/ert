<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;

class PredictionController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$query = $request->all();

		return $query;
	}
	
	public function columnName(Request $request)
	{
		// input: columnNames, columnIndex, tableName
		// output: columns [probability,name] (at least 3)
		$tableName = $request->post('tableName');
		$columnIndex = $request->post('columnIndex');
		$columnNames = $request->post('columnNames');
		return [
			['probability'=>0.9,	'name'=>'id'],
			['probability'=>0.8,	'name'=>'created_at'],
			['probability'=>0.8,	'name'=>'updated_at'],
			['probability'=>0.7,	'name'=>'title'],
		];	
	}

	public function columnType(Request $request)
	{
		// input: columnNames, columnIndex, tableName
		// output: columnTypes with params [probability,type,size,meta] (at least 5)
		$tableName = $request->post('tableName');
		$columnIndex = $request->post('columnIndex');
		$columnNames = $request->post('columnNames');
		return [
			['probability'=>0.9,	'type'=>'INTEGER',	'size'=>'14',	'meta'=>['UNSIGNED','PRIMARY'] ],
			['probability'=>0.5,	'type'=>'BIGINT',	'size'=>'20',	'meta'=>['UNSIGNED'] ],
		];	
	}

}
