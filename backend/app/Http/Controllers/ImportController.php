<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;

use iamcal\SQLParser as SQLParser;

class ImportController extends Controller
{

	public function text(Request $request)
  	{
		$sql = $request->get('sql');
		// @todo try and detect here from string which parser to use
		$parser = new SQLParser();
		$parser->parse($sql);

		$schema_struct = [
			'tables'		=> [],
			'connections'	=> [],
		];

		$table_struct = [
			"type"		=> "Table",
			'name'		=> "",
			'position'	=> ['x'=>0,'y'=>0],
			'columns'	=> [],
			'indexes'	=> [],
			'misc'		=> [],
		];

		$column_struct = [
			'name'			=> "",
            'type'			=> "",
            'size'			=> 0,
            'unsigned'		=> false,
            'nullable'		=> false,
			'autoincrement'	=> false,
			'default'		=> "",
			// missing more
		];

		$schema = $schema_struct;

		foreach($parser->tables as $pt){

			// table
			$table = $table_struct;
			$table['name'] = $this->cleanName($pt['name']);

			// columns
			foreach($pt['fields'] as $col){
				$column = $column_struct;
				$column['name'] = $this->cleanName($col['name']);
				$column['type'] = $this->cleanName($col['type']);
				$column['size'] = isset($col['length']) ? $col['length'] : 0;
				$column['unsigned'] = isset($col['unsigned']) ? $col['unsigned'] : false;
				$column['nullable'] = isset($col['null']) ? $col['null'] : false;
				$column['default'] = isset($col['default']) ? $col['default'] : "";
				$column['autoincrement'] = isset($col['auto_increment']) ? $col['auto_increment'] : false;
				// missing more
				$table['columns'][] = $column;
			}

			// indexes
			$table['indexes'] = $pt['indexes'] ? $pt['indexes'] : [];

			// misc
			$table['misc'] = $pt['props'] ? $pt['props'] : [];

			$schema['tables'][] = $table;
		}

		$response = [
			'success'	=> true,
			'schema'	=> $schema,
			'warnings'	=> [],
			'errors'	=> [],
		];
		return $response;
	}

	public function cleanName($str)
	{
		$str = str_replace(['"',"'","`","´"],'',$str);
		$str = trim($str);
		return $str;
	}
	
}
