<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;

class ExportController extends Controller
{
	public function toJson(Request $request)
	{
		$project = json_decode($request->get('project'));
		return $project;
	}

	public function toHtml(Request $request)
	{
		// $project = $request->get('project',[]);
		$project = Project::find(4);
		return view('export.html', ['project'=>$project]);

	}

	public function temp(Request $request)
	{
		$project = json_decode($request->get('project'));

		// @todo try and detect here from string which parser to use
		$parser = new SQLParser();
		$parser->parse($sql);

		$schema_struct = [
			'tables'		=> [],
			'connections'	=> [],
		];

		$table_struct = [
			"type"		=> "Table",
			'name'		=> "",
			'position'	=> ['x'=>0,'y'=>0],
			'columns'	=> [],
			'indexes'	=> [],
			'misc'		=> [],
		];

		$column_struct = [
			'name'			=> "",
            'type'			=> "",
            'size'			=> 0,
            'unsigned'		=> false,
            'nullable'		=> false,
			'autoincrement'	=> false,
			'default'		=> "",
			// missing more
		];

		$schema = $schema_struct;

		$response = [
			'success'	=> true,
		];
		return $response;
	}
}
