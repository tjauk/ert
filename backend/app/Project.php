<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = 'projects';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'schema', 'private'
    ];

    //
    public function getSchemaAttribute()
    {
        return json_decode($this->attributes['schema'],true);
    }
    public function setSchemaAttribute($json)
    {
        $this->attributes['schema'] = json_encode($json,JSON_UNESCAPED_UNICODE);
    }
}
